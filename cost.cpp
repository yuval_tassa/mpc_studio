#include "mpc_studio.h"

// scratch usage: nr+nr*nr+ndz+ndz*ndz+nr*ndz <= (nr+ndz+1)^2
mjtNum cost_term_derivs(trajElement *e, const mjtTerm *term, int ndx, int nu,
                mjtNum coef, mjtNum *scratch, int szScratch) {
  // constants
  int nr = term->nr, ndz = ndx + nu;
  mjtNum *r = e->r + term->r_adr, *rz = e->rz + ndz*term->r_adr;
  // variables
  int i;
  mjtNum c = 0;
  // mj_stack variables
  mjtNum *cr = 0, *crr = 0, *cz, *czz, *R;

  // quick return
  if (!coef) return 0;

  // ignore NaNs
  if (!term->isloss && tt_isBadVec(r, nr)) return 0;

  if (ndx) {
    if (!e->cx)
      mju_error("cost_term_derivs: no space for cost derivatives in trajectory");
    if (!scratch) mju_error("cost_term_derivs: scratch space pointer is NULL");
    cr = tt_stackAlloc(&scratch, &szScratch, nr);
    crr = tt_stackAlloc(&scratch, &szScratch, nr*nr);
    cz = tt_stackAlloc(&scratch, &szScratch, ndz);
    czz = tt_stackAlloc(&scratch, &szScratch, ndz*ndz);
    R = tt_stackAlloc(&scratch, &szScratch, nr*ndz);
    mju_zero(crr, nr*nr);
    mju_zero(cr, nr);
  }

  if (term->isloss)
    for (i = 0; i < nr; i++)
      if (mju_isBad(r[i])) {
        if (cr) mju_zero(rz + i*ndz, ndz);
        continue;
      } else
        c += mj_norm(cr ? cr + i : 0, crr ? crr + i*(nr + 1) : 0, r + i, 1,
                     term->params, term->norm);
  else
    c = mj_norm(cr ? cr : 0, crr ? crr : 0, r, nr, term->params, term->norm);

  // quick return
  if (!ndx) return coef*c;

  // gradient
  mju_mulMatTVec(cz, rz, cr, nr, ndz);

  // add gradients [cx cu] += coef*cz
  mju_addToScl(e->cx, cz, coef, ndx);
  mju_addToScl(e->cu, cz + ndx, coef, nu);

#ifdef ttVERBOSE
  if (tt_isBadVec(e->cx, ndx) || tt_isBadVec(e->cu, nu))
    mju_warning("(cost_term_derivs) cost gradient is bad");
#endif

  // Gauss-Newton Hessian
  if (term->isloss)  // crr is diagonal
    for (i = 0; i < nr; i++)
      mju_scl(R + i*ndz, rz + i*ndz, crr[i*(nr + 1)], ndz);
  else
    mju_mulMatMat(R, crr, rz, nr, nr, ndz);
  mju_mulMatTMat(czz, rz, R, nr, ndz, ndz);
  tt_symmetrize(czz, ndz);

  // add Hessians [cxx cxu; cxuT cuu] += coef*czz
  for (i = 0; i < ndx; i++)
    mju_addToScl(e->cxx + i*ndx, czz + i*ndz, coef, ndx);
  for (i = 0; i < ndx; i++)
    mju_addToScl(e->cxu + i*nu, czz + i*ndz + ndx, coef, nu);
  for (i = 0; i < nu; i++)
    mju_addToScl(e->cuu + i*nu, czz + i*ndz + ndx*ndz + ndx, coef, nu);

#ifdef ttVERBOSE
  if (tt_isBadVec(e->cxx, ndx*ndx) || tt_isBadVec(e->cxu, ndx*nu) ||
      tt_isBadVec(e->cuu, nu*nu))
    mju_warning("(cost_term_derivs) cost hessian is bad");
#endif

  return coef*c;
}


mjtNum cost_term(const mjtNum *r, const mjtTerm *term, mjtNum coef) {

  // quick return
  if (!coef) return 0;

  // constants
  int nr = term->nr;

  // variables
  int i;
  mjtNum c = 0;

  // ignore NaNs
  if (!term->isloss && tt_isBadVec(r, nr)) return 0;

  if (term->isloss)
    for (i = 0; i < nr; i++)
        c += mj_norm(0, 0, r + i, 1, term->params, term->norm);
  else
    c = mj_norm(0, 0, r, nr, term->params, term->norm);

  return coef*c;
}


mjtNum cost_step(const mjtNum *residual, const mjCost *cost, mjtNum dt) {
  int i;
  mjtNum c=0;

  // quick return
  if (!cost) return 0;

  for (i = 0; i < cost->nterms; i++)
    c += cost_term(residual+cost->term[i].r_adr, cost->term+i, cost->term[i].coef_running*dt);

  return c;
}

mjtNum cost_step_vec(const mjtNum *residual, const mjCost *cost, mjtNum dt, mjtNum *costvec) {
  int i;
  mjtNum ci, c=0;

  // quick return
  if (!cost) return 0;

  for (i = 0; i < cost->nterms; i++) 
  {
    ci = cost_term(residual+cost->term[i].r_adr, cost->term+i, cost->term[i].coef_running*dt);
    if (costvec) costvec[i] = ci;
    c += ci;
  }

  return c;
}


mjtNum cost_step_derivs(trajElement *e, const mjCost *cost, int nx, int ndx, int nu,
                int nr, mjtNum t0, mjtNum *scratch, int szScratch) {
  int i, j, ni, adr;
  mjtNum coef, c, *cr, *crr;

  // set derivatives to zero, cost_term_derivs() is cumulative
  if (e->cx && ndx) {
    mju_zero(e->cx, ndx);
    mju_zero(e->cxx, ndx*ndx);

    mju_zero(e->cxu, ndx*nu);

    mju_zero(e->cu, nu);
    mju_zero(e->cuu, nu*nu);
  }

  // quick return
  if (!cost) return 0;

  for (i = 0; i < cost->nterms; i++) {
    coef = cost->term[i].coef_running*e->dt[0];

    e->c[i] = cost_term_derivs(e, cost->term + i, ndx, nu, coef, scratch, szScratch);

    //write into cr and crr
    if (e->cr && e->crr)
    {
      adr = cost->term[i].r_adr;
      ni  = cost->term[i].nr;
      cr  = scratch;
      crr = scratch+ni;    
    
      mju_copy(e->cr+adr, cr, ni);

      for (j = 0; j < ni; j++)
        mju_copy(e->crr + nr*(adr+j)+adr, crr+j*ni, ni);
    }
  }

  c = 0;
  for (i = 0; i < cost->nterms; i++) c += e->c[i];

#ifdef ttVERBOSE
  if (mju_isBad(c)) mju_warning("(cost_step_derivs) cost is bad");
#endif
  return c;
}

/*   new versions of cost functions, not using trajectory

mjtNum cost_term_derivs2(const mjtTerm *term, const mjtNum *r, mjtNum coef, const mjtNum *rx, int nx, 
                          mjtNum *cx, mjtNum *cr, mjtNum *cxx, mjtNum *crr, mjtNum *scratch_rx)
{
  int nr = term->nr;
  int i;
  mjtNum c = 0;

  // quick return
  if (!coef) return 0;


  if (term->isloss){
    if (crr) mju_zero(crr, nr*nr);
    for (i = 0; i < nr; i++)
        c += mj_norm(cr ? cr+i : 0, crr ? crr+i*(nr+1) : 0, r+i, 1, term->params, term->norm);
  }else
    c = mj_norm(cr ? cr : 0, crr ? crr : 0, r, nr, term->params, term->norm);

  // quick return
  if (!nx) return coef*c;

  // gradient
  mju_mulMatTVec(cx, rx, cr, nr, nx);
  mju_scl(cx, cx, coef, nx);

  // Gauss-Newton Hessian
  if (term->isloss)  // crr is diagonal
    for (i = 0; i < nr; i++)
      mju_scl(scratch_rx + i*nx, rx + i*nx, crr[i*(nr + 1)], nx);
  else
    mju_mulMatMat(scratch_rx, crr, rx, nr, nr, nx);

  mju_mulMatTMat(cxx, rx, scratch_rx, nr, nx, nx);
  tt_symmetrize(cxx, nx);
  mju_scl(cxx, cxx, coef, nx*nx);

  return coef*c;
}


mjtNum cost_term2(const mjtNum *r, const mjtTerm *term, mjtNum coef) {
  return cost_term_derivs2(term, r, coef, 0, 0, 0, 0, 0, 0, 0);
}


// mjtNum cost_term_derivs2(const mjtNum *r, const mjtTerm *term, mjtNum coef, const mjtNum *rx, int nx, 
//                           mjtNum *cx, mjtNum *cr, mjtNum *cxx, mjtNum *crr, mjtNum *scratch_rx)
// mjtNum cost_step_vec(const mjtNum *residual, const mjCost *cost, mjtNum dt, mjtNum *costvec)

mjtNum cost_step_derivs2(const mjtNum *r, const mjCost *cost, const mjtNum *rx, mjtNum dt, int nx, 
                         mjtNum *cx, mjtNum *cxx, mjtNum *cr, mjtNum *crr, 
                         mjtNum *scratch, int szScratch) {
  int i, j, ni, adr, nr=0;
  mjtNum coef, c, *ri;

  // quick return
  if (!cost) return 0;

  // set derivatives to zero, cost_term_derivs() is cumulative
  mju_zero(cx, nx);
  mju_zero(cxx, nx*nx);

  // count nr
  for (i = 0; i < cost->nterms; i++) 
    nr += cost->term[i].nr;

  for (i = 0; i < cost->nterms; i++) 
  {
    coef = cost->term[i].coef_running*dt;
    adr  = cost->term[i].r_adr;
    ni   = cost->term[i].nr;

    c[i] = cost_term_derivs2(r+adr, cost->term[i], coef, rx + nx*adr, nx, cx, cr+adr, cxx, scratch, scratch+nr*nr);

    //write into cr and crr
    if (e->cr && e->crr)
    {     
      cr  = scratch;
      crr = scratch+ni;    
    
      mju_copy(e->cr+adr, cr, ni);

      for (j = 0; j < ni; j++)
        mju_copy(e->crr + nr*(adr+j)+adr, crr+j*ni, ni);
    }
  }

  c = 0;
  for (i = 0; i < cost->nterms; i++) 
    c += e->c[i];


  return c;
}
*/
