#ifndef _TT_NORM_H_
#define _TT_NORM_H_
#include "util.h"

#define STR_LENGTH 20
#define MAX_NORM_PARAM 4

#define NUM_NORM 23
typedef enum _mjtNormType {
  // losses (take scalar as input):
  mjLOSS_IDENTITY,     // identity, 			       y = x
  mjLOSS_QUADRATIC,    // quadratic, 			       y = 0.5*x^2
  mjLOSS_POWER,        // power, 				         y = abs(x)^p
  mjLOSS_EXP,          // exponential, 		       y = exp(p*x))
  mjLOSS_SMOOTH_ABS,   // smooth_abs, 			     y = sqrt(x^2 + p^2) - p
  mjLOSS_SMOOTH_ABS2,  // smooth_abs2, 		       y = (abs(x)^q + p^q)^(1/q) - p
  mjLOSS_HUBER,        // huber,                 y = abs(x)<p ? x^2/p/2 : abs(x)-p/2
  mjLOSS_HUBER2,       // huber2,                y = abs(x)<p ? abs(x)^q/p^(q-1)/q : abs(x)-p*(q-1)/q
  mjLOSS_RECTIFY,      // rectify, 			         y = (x + sqrt(x^2 + p^2)) / 2
  mjLOSS_RECTIFY2,     // rectify2, 			       y = (q*x + sqrt(x^2 + p^2)) / 2,
  mjLOSS_QUAD_BIAS,    // quadratic with bias,   y = 0.5*(x-p)^2
  mjLOSS_SIGMOID,      // long-tailed sigmoid,   y = x/sqrt(x^2+beta^2)
  mjLOSS_STEP,         // step function,		     y = (1+sigmoid(x))/2
  mjLOSS_CLIP,         // clip
  mjLOSS_SINE,         // sine					         y = sin(x)
  mjLOSS_COSINE,       // cosine				         y = cos(x)
  mjLOSS_LOG,          // log					           y = log(x)
  // norms (take vector as input):
  mjNORM_SUM,          // identity, 			       y = sum(x)
  mjNORM_PROD,         // product, 			         y = prod(x)
  mjNORM_BILINEAR,     // bilinear, 			       y = sum(x(0:n/2-1).*x(n/2:end))
  mjNORM_L2,           // smooth L2, 			       y = sqrt(x*x' + p^2) - p
  mjNORM_L22,          // L22, 				           y = ((x*x')^q + p^(2*q))^(1/2/q) - p
  mjNORM_GAUSSIAN,     // gaussian norm,		     y = exp(sum(-(x/p)^2/2))
} mjtNormType;

mjtNum mj_norm(mjtNum *g, mjtNum *H, const mjtNum *z, int n, const mjtNum *params,
              mjtNormType type);

typedef struct _mjtNorm {
  char name[20];
  mjtNormType type;
  mjtByte isloss;
  int nparam;
  int dim;
} mjtNorm;

#endif
