#ifndef _TT_USER_COST_H_
#define _TT_USER_COST_H_

#include "mujoco.h"

#include "features.h"
#include <vector>
#include <string>

class mjCFC;

class ttBase {
 public:
  std::string name;  // object name
  int id;            // object id
  int xmlpos[2];     // row and column in xml file
  const mjModel* m;  // pointer to model that created object

 protected:
  ttBase() {
    name.clear();
    id = -1;
    xmlpos[0] = xmlpos[1] = -1;
    m = 0;
  }
};
// error information
class ttError {
 public:
  ttError(const ttBase* obj = 0, const char* msg = 0, const char* str = 0,
          int pos1 = 0, int pos2 = 0);

  char message[500];  // error message
  bool warning;       // is this a warning instead of error
};
class mjFCBase : public ttBase {
 public:
  mjCFC* fc;
};

//------------------------- class mjCFeatureset
//-------------------------------------------
// Describes a featureset

typedef enum _mjtFEATURETYPE {
  mjFIELD_DATA_MEMBER,
  mjFIELD_BUFFER,
  mjFIELD_CONTACT,
  mjFIELD_ON_DEMAND
} mjtFEATURETYPE;

/*

// key(std::string) : value(int) map
typedef struct _mjContactMap
{
        std::string			key;
        mjtContactField	field;
        int 			entries;
} mjContactMap;

static const int contactFields_sz = 6;
static const mjContactMap contactFields_map[contactFields_sz] = {
        { "dist",				mjfDIST,
1},
        { "ndist",				mjfNDIST,
1},
        { "pos",				mjfPOS, 3},
        { "frame",				mjfFRAME,
9},
        { "normal",				mjfNORMAL,
1},
        { "friction",			mjfFRICTION,		1},
};
*/
// key(std::string) : value(int) map
typedef struct _mjEntryMap {
  std::string key;
  int value;
  int size;
  int group;
} mjEntryMap;

static const int entry_sz = 27;
static const mjEntryMap entry_map[entry_sz] = {
    {"x", 0, 3, 0},      {"y", 1, 3, 0},      {"z", 2, 3, 0},
    {"xx", 0, 9, 1},     {"xy", 1, 9, 1},     {"xz", 2, 9, 1},
    {"yx", 3, 9, 1},     {"yy", 4, 9, 1},     {"yz", 5, 9, 1},
    {"zx", 6, 9, 1},     {"zy", 7, 9, 1},     {"zz", 8, 9, 1},
    {"wx", 0, 6, 2},     {"wy", 1, 6, 2},     {"wz", 2, 6, 2},
    {"vx", 3, 6, 2},     {"vy", 4, 6, 2},     {"vz", 5, 6, 2},
    {"qw", 0, 4, 3},     {"qx", 1, 4, 3},     {"qy", 2, 4, 3},
    {"qz", 3, 4, 3},     {"ref", 0, 0, 0},    {"coef", 1, 0, 0},
    {"param0", 2, 0, 0}, {"param1", 3, 0, 0}, {"all", 99, -1, -1}};

class mjCFeatureComponent : public mjFCBase {
 public:
  virtual ~mjCFeatureComponent() = 0;
  virtual int Compile(const mjModel* m) = 0;  // returns output's length
};
/*
class mjCContact  : public mjCFeatureComponent
{
public:
        explicit mjCContact(mjCModel* usermodel);
        ~mjCContact();
        int Compile(mjModel* m);

        // read from the XML file
        std::string	fieldname;
        std::string	geom1name;
        std::string	geom2name;
        std::string	body1name;
        std::string	body2name;
        std::string	entrynames;
        std::string	selectorname;
        vector<double>	coefs;
        vector<double>  refs;

        // processed by compile(), read by interpreter
        mjtFEATURE_FIELD field;
        mjtObj			itemtype;
        vector<int>		items;
        vector<int>		entries;
        int				extra_flags;
        vector<mjDatum*> datums;
};
*/
class mjCDatums : public mjCFeatureComponent {
 public:
  mjCDatums(const mjModel* _m, mjCFC* _fc);
  ~mjCDatums();
  int Compile(const mjModel* m);

  // read from the XML file
  std::string fieldname;
  std::string itemnames;
  std::string entrynames;
  std::vector<double> coefs;
  std::vector<double> refs;

  // processed by compile(), read by interpreter
  mjtFEATURE_FIELD field;
  mjtObj itemtype;
  std::vector<int> items;
  std::vector<int> entries;
  int extra_flags;
  std::vector<mjDatum*> datums;
};

typedef enum _normloss { mjNL_GENERIC, mjNL_NORM, mjNL_LOSS } mjtNormLoss;

class mjCOperator : public mjCFeatureComponent {
 public:
  explicit mjCOperator(const mjModel* _m, mjCFC* _fc);
  ~mjCOperator();
  int Compile(const mjModel* m);

  // read directly from XML
  double coef;
  double ref;
  std::string funname;
  std::vector<double> params;

  // processed by compile
  mjCFeatureComponent* in1;
  int in1_adr;
  int in1_size;

  mjCFeatureComponent* in2;
  int in2_adr;
  int in2_size;

  mjtNormLoss lossNorm;  // wether fun is a loss, norm, or generic function
  mjtOperatorType fun;
  int out_adr;  // address in residual's feat index
  int out_size;
};

class mjCFeature : public mjFCBase {
 public:
  mjCFeature(const mjModel*, mjCFC*);
  ~mjCFeature();
  void Compile(const mjModel* m);

  mjCFeatureComponent* component;
  int adr;
  int size;
};

class mjCDatafileColumn : public ttBase {
 public:
  int col;
  int n;
  int row_offset;
  void Compile();
  mjCDatafileColumn(const mjModel*, mjCFC*);
  //~mjCDatafileColumn();
};

//------------------------- class mjCCost_term
//-------------------------------------------

class mjCCost_term : public mjFCBase {
  friend class mjCModel;

 public:
  std::string featurename;
  std::string normname;

  mjtNormType normid;
  bool isloss;
  // mjCFeature_set* feature_set;
  double coef[2];
  std::vector<double> params;
  int nr;
  int r_adr;
  int f_inx;
  ~mjCCost_term();
  mjCCost_term(const mjModel* _m, mjCFC* _fc);
  void Compile(int _id);
};
//------------------------- class mjCOverwrite
//-------------------------------------------

typedef enum _modelFields {
  mjMF_ARMATURE,
  mjMF_GEOM_POS,
  mjMF_QPOS_SPRING,
  mjMF_STIFFNESS,
  mjMF_TENDON_SPRING,
  mjMF_TENDON_STIFFNESS,
  mjMF_FEATURE,
} modelFields;

typedef struct _mjModelMap {
  std::string key;
  modelFields field;
  int entries;
  mjtObj item_type;

} mjModelMap;

static const int modelFields_sz = 7;
static const mjModelMap modelFields_map[modelFields_sz] = {
    {"armature", mjMF_ARMATURE, 1, mjOBJ_JOINT},
    {"geom_pos", mjMF_GEOM_POS, 3, mjOBJ_GEOM},
    {"spring_ref", mjMF_QPOS_SPRING, 1, mjOBJ_JOINT},
    {"stiffness", mjMF_STIFFNESS, 1, mjOBJ_JOINT},
    {"tendon_spring_ref", mjMF_TENDON_SPRING, 1, mjOBJ_TENDON},
    {"tendon_stiffness", mjMF_TENDON_STIFFNESS, 1, mjOBJ_TENDON},
    {"feature", mjMF_FEATURE, 1, mjOBJ_UNKNOWN}};

class mjCIgnore : public ttBase {
 public:
  int itemid;
  int type;
  std::string entryname;
  int posvel;
  int entry;
  mjCIgnore(const mjModel* _m);
  void Compile();

 private:
  int entrytype;
};

//------------------------- class mjCTransition
//-------------------------------------------

class mjCTransition : public mjFCBase {
 public:
  std::string to_name;
  std::string feature_name;
  std::string normname;

  mjCFeature* feature;
  int to_id;

  mjtNormType normid;
  bool isloss;

  std::vector<double> params;
  double threshold;
  int nr;
  int adr;
  int f_inx;
  mjCTransition(const mjModel* _m, mjCFC* _fc);
  ~mjCTransition();
  void Compile(bool dataTransition);
};

//------------------------- class mjCAlter
//-------------------------------------------

class mjCAlter : public ttBase {
 public:
  std::string fieldname;
  std::string itemname;
  std::string feature_name;
  mjCAlter(const mjModel* _m);
  void Compile(const mjModel* m);

  // processed by compile
  modelFields field;
  int item;
  mjtObj itemtype;
  int buffer_offset;
  mjCFeature* feature;
  int nr;
  int adr;  // for the old formulation
  int f_inx;
};

//------------------------- class mjCCost
//-------------------------------------------

class mjCCost : public ttBase {
 public:
  std::vector<mjCCost_term*> terms;
  std::vector<mjCTransition*> transitions;
  std::vector<mjCAlter*> alters;
  std::string plot_feature_name;
  int plot_f_inx;
  mjOption opt;
  mjOptOpt optopt;
  mjCCost(const mjModel*, mjCFC*);
  mjCTransition* datafiletransition;

  void Compile_terms();
  void Compile_transitions_alters(const mjModel* m);
  ~mjCCost();
};

// ----------------------- tweaks -----------------------

class mjCTweak : public mjFCBase {
 public:
  std::string fieldname;
  std::string itemname;
  std::string entryname;
  modelFields field;
  int item;   // if feature, this is f_inx
  int entry;  // if feature, this is enum in the set {ref, coef, param0, param1,
              // ...}
  bool slider;  // if false, make a textbox
  float minval;
  float maxval;

  mjCTweak(const mjModel* _m, mjCFC* _fc);
  void Compile();
};

class mjCFC : public ttBase {
 public:
  mjCFC();
  ~mjCFC();

  mjCFeature* AddFeature(std::string = "");
  mjCCost* AddCost(std::string = "");
  void compileRC(const mjModel* m, const mjData* data);
  mjResidual residual;  // cost is in here
  std::vector<mjCFeature*> features;
  std::vector<mjCCost*> CCosts;

  // Features Machinery
  std::vector<std::string> observationFeatureNames;
  std::vector<int> observationFeatureIndices;

  // Costs Machineary
  std::vector<std::string> costNames;
  
  // vector<mjCIgnore*> ignores;
  // mjCIgnore* AddIgnore();
  std::string default_plot_feature_name;
  std::string datafile;
  int rowlength;
  int min_row_offset;
  int max_row_offset;
  std::vector<mjCDatafileColumn*> datafile_columns;
  mjCDatafileColumn* AddDatafileColumn(std::string = "");
  std::vector<mjCTweak*> tweaks;
  mjCFeature* FindFeature(std::string featureName);
  mjCDatafileColumn* FindDatafileColumn(std::string name);
  mjCCost* FindCost(std::string name);

 private:
  template <class T>
  T* AddObject(std::vector<T*>& list, std::string name);
};

// = = = = = = = = = = = = = = = = = = =

mjtTerm make_term(mjCCost_term* ct);
void make_cost(mjCost* cost, mjCCost* cc, mjCFC* fc, const mjModel* m);
void interpret_features(mjResidual* res, std::vector<mjCFeature*> fsv);

#endif  // _TT_USER_COST_H_
