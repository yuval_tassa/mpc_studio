#ifndef _TT_COST_H_
#define _TT_COST_H_


#include "norm.h"
#include "trajectory.h"

#define MAX_N_TERM 32
#define MAX_N_TRANSITIONS 3
#define MAX_N_ALTERS 50

// ========= callback types =========
typedef int (*rollout_fun)(void *cbData, const trajectory *policy,
                           trajectory **candidates);
typedef int (*derivative_fun)(void *cbData, trajectory *T);

typedef enum _ttVerbosity {
  verboseSILENT = 0,
  verboseWARNING,
  verboseFINAL,
  verboseITER,
  verboseDEBUG
} ttVerbosity;

typedef struct _mjOptOpt {
  // general
  ttVerbosity verbosity;

  // parallelization
  int nThreads;  // number of threads

  // trajectory options
  int N;            // horizon length
  mjtNum discount;   // discounting
  mjtNum dt_start;   // size of initial timestep
  int n_dt_start;   // number of timesteps to use dt_start
  int n_dt_interp;  // number of timesteps to interpolate from dt_start to dt

  // findiff options
  int log2eps;         // log2(epsilon)
  int central;         // 0: forward; 1: central
  int extrap_lc_dist;  // skip collision detection, extrapolate distances
  int iter_flc_f;      // number of iterations for flc_f in cloud points
  int act_jac;         // use analytic Jacobian for act variables

  // Levenberg-Marquardt options
  int maxIter;        // maximum number of iterations
  mjtNum muInit;       // initial value for mu   yuval: why?
  mjtNum muMin;        // minimum value for mu
  mjtNum muMax;        // above this value is considered failure
  mjtNum muFactor;     // coefficient for changing dmu
  mjtNum zBad;         // below this value increase mu
  mjtNum zGood;        // above this value decrease mu
  mjtNum alphaBad;     // above this value decrease mu
  mjtNum alphaGood;    // above this value decrease mu
  mjtNum minAlpha;     // smallest line-search alpha
  mjtNum tolX;         // exit tolerance: change of x
  mjtNum tolG;         // exit tolerance: norm of gradient
  mjtNum tolFun;       // exit tolerance: change of objective function
  mjtByte sparseHess;  // Hessian with arrow-shaped sparsity pattern

  // iLQG options
  int maxBpass;  // maximum number of backpasses
  int nRollout;  // number of rollouts
  int regType;   // regularization type

  // estimation options
  mjtNum procNoise;  // maximum number of backpasses
  mjtNum obsNoise;   // number of rollouts

  // callbacks
  rollout_fun cb_rollout;
  derivative_fun cb_derivatives;
} mjOptOpt;

typedef struct _mjtTerm {
  // char	name[STR_LENGTH];
  int nr;
  int f_inx;
  int r_adr;
  mjtNormType norm;
  mjtByte isloss;
  mjtNum params[MAX_NORM_PARAM];
  mjtNum coef_running;
} mjtTerm;

typedef struct _mjtTransition {
  int nr;
  int f_inx;
  int f_adr;
  mjtNormType norm;
  mjtByte isloss;
  mjtNum params[MAX_NORM_PARAM];
  mjtNum threshold;
  int to_cost;
} mjtTransition;

typedef struct _mjtAlter {
  int nr;
  int f_inx;
  int f_adr;
  int buffer_offset;
} mjtAlter;

typedef struct _mjCost {
  int nr;
  int n_auxf;
  int plot_f_inx;
  int plot_nf;
  int nterms;
  int ntransitions;
  int nalters;
  mjtNum entry_time;
  mjtTerm term[MAX_N_TERM];
  mjtTransition transition[MAX_N_TRANSITIONS];
  mjtAlter alter[MAX_N_ALTERS];
  mjtTransition dataTransition;
  //mjOptOpt optopt;
} mjCost;

void muControl(mjoTrace *t, const mjOptOpt o);
void muScale(mjoTrace *t, const mjOptOpt o, mjtNum factor);

extern mjtNorm funList[NUM_NORM];

void defaultOptOpt(mjOptOpt *optopt);
mjtNum cost_step_derivs(trajElement *e, const mjCost *cost, int nx, int ndx, int nu,
                int nr, mjtNum t0, mjtNum *scratch, int szScratch);
mjtNum cost_step(const mjtNum *residual, const mjCost *cost, mjtNum dt);
mjtNum cost_step_vec(const mjtNum *residual, const mjCost *cost, mjtNum dt, mjtNum *costvec);


#endif  // _TT_COST_H_
