#include "mpc_studio.h"

#include <memory>
#include <iostream>
#include <sstream>
#include <fstream>
#include <typeinfo>
#include <vector>

// #include "user_cost.h"

// get number of objects and name addresses for given object type
static int _getnumadr(const mjModel* m, mjtObj type, int** padr) {
  // get address list and size for object type
  switch (type) {
    case mjOBJ_BODY:
      *padr = m->name_bodyadr;
      return m->nbody;
      break;

    case mjOBJ_JOINT:
      *padr = m->name_jntadr;
      return m->njnt;
      break;

    case mjOBJ_GEOM:
      *padr = m->name_geomadr;
      return m->ngeom;
      break;

    case mjOBJ_SITE:
      *padr = m->name_siteadr;
      return m->nsite;
      break;

    case mjOBJ_CAMERA:
      *padr = m->name_camadr;
      return m->ncam;
      break;

    case mjOBJ_MESH:
      *padr = m->name_meshadr;
      return m->nmesh;
      break;

    case mjOBJ_HFIELD:
      *padr = m->name_hfieldadr;
      return m->nhfield;
      break;

    case mjOBJ_EQUALITY:
      *padr = m->name_eqadr;
      return m->neq;
      break;

    case mjOBJ_TENDON:
      *padr = m->name_tendonadr;
      return m->ntendon;
      break;

    case mjOBJ_ACTUATOR:
      *padr = m->name_actuatoradr;
      return m->nu;
      break;

    case mjOBJ_NUMERIC:
      *padr = m->name_numericadr;
      return m->nnumeric;
      break;

    case mjOBJ_TEXT:
      *padr = m->name_textadr;
      return m->ntext;
      break;

    default:
      *padr = 0;
      return 0;
  }
}

template <class T>
static void processlist(std::vector<T*>& list, std::string defname,
                        bool checkrepeat = true) {
  // loop over list elements
  for (int i = 0; i < (int)list.size(); i++) {
    // check for incompatible id setting
    if (list[i]->id != -1 && list[i]->id != i)
      throw ttError(list[i], "incompatible id in %s array, position %d",
                    defname.c_str(), i);

    // id equals position in array
    list[i]->id = i;

    // compare to all previous names
    if (checkrepeat)
      for (int j = 0; j < i; j++)
        if (list[i]->name == list[j]->name && list[j]->name != "")
          throw ttError(list[i], "repeated name in %s array, position %d",
                        defname.c_str(), i);
  }
}

// add object of any type
template <class T>
T* mjCFC::AddObject(std::vector<T*>& list, std::string name) {
  // allocate object
  T* obj = new T(m, this);

  // set name and id
  obj->name = name;
  obj->id = (int)list.size();

  list.push_back(obj);
  return obj;
}

mjCFeature* mjCFC::AddFeature(std::string name) {
  return AddObject(features, name);
}

mjCDatafileColumn* mjCFC::AddDatafileColumn(std::string name) {
  return AddObject(datafile_columns, name);
}

mjCCost* mjCFC::AddCost(std::string name) { return AddObject(CCosts, name); }

template <class T>
static T* findobject(std::string name, std::vector<T*>& list) {
  for (unsigned int i = 0; i < list.size(); i++)
    if (list[i]->name == name) return list[i];

  return 0;
}

mjCFeature* mjCFC::FindFeature(std::string name) {
  return findobject(name, features);
}

mjCCost* mjCFC::FindCost(std::string name) { return findobject(name, CCosts); }

mjCDatafileColumn* mjCFC::FindDatafileColumn(std::string name) {
  return findobject(name, datafile_columns);
}

mjCFC::mjCFC() {
  // Tom's block
  features.clear();
  datafile_columns.clear();

  // Features Machinery
  observationFeatureNames.clear();
  observationFeatureIndices.clear();

  // Costs Machinery
  costNames.clear();

  CCosts.clear();
  rowlength = 0;
  tweaks.clear();
}

mjCFC::~mjCFC() {
  int i;
  for (i = 0; i < CCosts.size(); i++) delete CCosts[i];
  CCosts.clear();

  for (i = 0; i < features.size(); i++) delete features[i];
  features.clear();

  for (i = 0; i < tweaks.size(); i++) delete tweaks[i];
  tweaks.clear();
}


void mjCFC::compileRC(const mjModel* model, const mjData* data) {
  processlist(features, "features");
  processlist(CCosts, "costs");

  if (CCosts.size() > MAX_N_COSTS) mju_error("too many cost functions! (currently hard-coded to as MAX_N_COSTS==5 at features.h)");

  int i;

  // handle cost, features:
  residual.extra_flags = 0;
  residual.num_features = 0;
  for (i = 0; i < features.size(); i++) features[i]->Compile(model);
  interpret_features(&residual, features);

  for(std::vector<std::string>::size_type i = 0;
	  i != features.size(); i++) {
	  mjCFeature* f = features[i];
    residual.observationLengths[i] = f->size;
    residual.observationIndices[i] = f->adr;
  	strcpy(residual.observationNames[i], f->name.c_str());
  	if (i >= MAX_OBS) {
      mju_error("increase MAX_OBS!");
    }
  }
  residual.numObservations = features.size();

  residual.numCosts = 0;
  for (i = 0; i < CCosts.size(); i++) {
    residual.numCosts += 1;
    make_cost(residual.cost + i, CCosts[i], this, model);
    strcpy(residual.costNames[i], costNames[i].c_str());
  }
  residual.ncosts = (int)CCosts.size();

  residual.active_cost = 0;

  //  get max_nterms and max_nr
  residual.max_nr = 0;
  residual.max_nterms = 0;
  residual.n_auxf = 0;
  for (i = 0; i < residual.ncosts; i++) {
    if (residual.cost[i].nterms > residual.max_nterms)
      residual.max_nterms = residual.cost[i].nterms;
    if (residual.cost[i].nr > residual.max_nr)
      residual.max_nr = residual.cost[i].nr;
    if (residual.cost[i].n_auxf > residual.n_auxf)
      residual.n_auxf = residual.cost[i].n_auxf;
  }

  for (i = 0; i < tweaks.size(); i++) tweaks[i]->Compile();


  m = model;

  /*
  // test forward simulation
  double temp[MAX_NR];
  get_residual(temp, m, residual, data);

  // test serialization, de-serialization:
  int szbuf = 10000;
  void* buf = mju_malloc(szbuf);
  mj_saveFC(residual, tcost, 0, szbuf, buf);

  mjResidual* r2=0;
  mjCost* c2=0;
  mj_loadFC(&r2, &c2, 0, szbuf, buf);
  */

  residual.rowlength = rowlength;
  residual.currowstart = 0;
  if (rowlength) {
    std::ifstream strm(datafile.c_str());
    if (strm.fail())
      throw ttError();  // mjERR_CUSTOMSIZE, this, "datafile not found");

    mjtNum temp;
    std::vector<mjtNum> vec;
    while (!strm.eof()) {
      strm >> temp;
      vec.push_back(temp);
    }
    residual.pfiledata = (mjtNum*)mju_malloc((int)vec.size() * sizeof(mjtNum));
    for (i = 0; i < vec.size(); i++) residual.pfiledata[i] = vec[i];
    residual.filesize = (int)vec.size();
    residual.currowstart = -min_row_offset;
  } else
    residual.pfiledata = 0;
}


mjtTerm make_term(mjCCost_term* ct, int adr) {
  mjtTerm term;

  term.nr = ct->nr;
  term.coef_running = ct->coef[0];
  term.norm = ct->normid;
  term.isloss = ct->isloss;
  term.f_inx = ct->f_inx;
  term.r_adr = adr;
  for (int i = 0; i < ct->params.size(); i++) term.params[i] = ct->params[i];

  return term;
}

mjtTransition make_transition(mjCTransition* ct, int adr) {
  mjtTransition term;

  term.nr = ct->nr;
  // new formulation!
  term.f_adr = adr;
  if (ct->f_inx > -1)
    // a real feature
    term.f_inx = ct->f_inx;
  else  // timeout
    term.f_inx = -1;
  term.norm = ct->normid;
  term.isloss = ct->isloss;
  for (int i = 0; i < ct->params.size(); i++) term.params[i] = ct->params[i];
  term.threshold = ct->threshold;
  term.to_cost = ct->to_id;

  return term;
}

mjtAlter make_alter(mjCAlter* ct, int adr) {
  mjtAlter alter;

  alter.nr = ct->nr;
  alter.f_inx = ct->f_inx;
  if (ct->adr > -1)
    alter.f_adr = ct->adr;
  else
    alter.f_adr = adr;
  alter.buffer_offset = ct->buffer_offset;

  return alter;
}





void make_cost(mjCost* cost, mjCCost* cc, mjCFC* fc, const mjModel* m) {
  std::string feature_name;
  if (cc->plot_feature_name.length()) {
    feature_name = cc->plot_feature_name;
  } else {
    feature_name = fc->default_plot_feature_name;
  }

  if (feature_name.length()) {
    // find that feature
    mjCFeature* feature = fc->FindFeature(feature_name);
    if (!feature)
      throw ttError(cc, "feature name %s not found for plotting",
                    feature_name.c_str());
    cost->plot_f_inx = feature->adr;
    cost->plot_nf = feature->size;
  } else {
    cost->plot_f_inx = -1;
    cost->plot_nf = 0;
  }

  cc->Compile_terms();

  //cost->optopt = cc->optopt;
  cost->nterms = (int)cc->terms.size();
  if (cost->nterms > MAX_N_TERM)
    throw ttError(cc, "%s too many cost terms %d (max is %d)", cc->name.c_str(),
                  cost->nterms, MAX_N_TERM);
  int adr = 0;
  for (int i = 0; i < cost->nterms; i++) {
    cost->term[i] = make_term(cc->terms[i], adr);
    adr = cost->term[i].r_adr + cost->term[i].nr;
  }

  cost->nr = adr;  // total

  cost->ntransitions = (int)cc->transitions.size();
  if (cost->ntransitions > MAX_N_TRANSITIONS)
    throw ttError(cc, "%s too many cost transitions %d (max is %d)",
                  cc->name.c_str(), cost->ntransitions, MAX_N_TRANSITIONS);

  adr = cost->plot_nf;
  for (int i = 0; i < cost->ntransitions; i++) {
    cc->transitions[i]->Compile(false);
    cost->transition[i] = make_transition(cc->transitions[i], adr);
    adr = cost->transition[i].f_adr + cost->transition[i].nr;
  }

  cost->nalters = (int)cc->alters.size();
  if (cost->nalters > MAX_N_ALTERS)
    throw ttError(cc, "%s too many cost alters %d (max is %d)",
                  cc->name.c_str(), cost->nalters, MAX_N_ALTERS);
  for (int i = 0; i < cost->nalters; i++) {
    cc->alters[i]->Compile(m);
    cost->alter[i] = make_alter(cc->alters[i], adr);
    adr = cost->alter[i].f_adr + cost->alter[i].nr;
  }

  if (fc->rowlength) {
    cc->datafiletransition->Compile(true);
    cost->dataTransition = make_transition(cc->datafiletransition, adr);
    adr = cost->dataTransition.f_adr + cost->dataTransition.nr;
  } else {
    cost->dataTransition.f_inx = -1;
    cost->dataTransition.f_adr = -1;
  }
  cost->n_auxf = adr;
}

//------------------ features ---------------------------------
/*
mjCContact::mjCContact(mjModel* _m)
{
        // set model pointer
        model = _model;
        coefs.clear();
        refs.clear();

        selectorname.clear();
        entrynames.clear();
        fieldname.clear();
        geom1name.clear();
        geom2name.clear();
        body1name.clear();
        body2name.clear();


}

mjCContact::~mjCContact()
{
        for( int i=0; i<datums.size(); i++ )
                delete datums[i];
        datums.clear();
        coefs.clear();
        refs.clear();
}

int mjCContact::Compile(mjModel *m) {

        return 0;
}

*/
mjCFeatureComponent::~mjCFeatureComponent() {}
mjCDatums::mjCDatums(const mjModel* _m, mjCFC* _fc) {
  // set model pointer
  fc = _fc;
  m = _m;
  extra_flags = 0;
  coefs.clear();
  refs.clear();
  items.clear();
  entries.clear();
  datums.clear();
}

mjCDatums::~mjCDatums() {
  for (int i = 0; i < datums.size(); i++) delete datums[i];
  datums.clear();
  coefs.clear();
  refs.clear();
}

#define MJCOMPUTED_FIELDS             \
  X(mjtNum, dt, nv - m->nv + 1, 1)     \
  X(mjtNum, time, nv - m->nv + 1, 1)   \
  X(mjtNum, com, nv - m->nv + 1, 3)    \
  X(mjtNum, energy, nv - m->nv + 1, 2) \
  X(mjtNum, xvel, nbody, 6)            \
  X(mjtNum, geom_xvel, ngeom, 6)       \
  X(mjtNum, site_xvel, nsite, 6)       \
  X(mjtNum, xvel_local, nbody, 6)      \
  X(mjtNum, geom_xvel_local, ngeom, 6) \
  X(mjtNum, site_xvel_local, nsite, 6) \
  X(mjtNum, xacc, nbody, 6)            \
  X(mjtNum, geom_xacc, ngeom, 6)       \
  X(mjtNum, site_xacc, nsite, 6)       \
  X(mjtNum, xacc_local, nbody, 6)      \
  X(mjtNum, geom_xacc_local, ngeom, 6) \
  X(mjtNum, site_xacc_local, nsite, 6) \
  X(mjtNum, subcom, nbody, 3)          \
  X(mjtNum, subcomvel, nbody, 3)       \
  X(mjtNum, momentum, nbody, 6)        \
  X(mjtNum, sqrtminv, nv, m->nv)       \
  X(mjtNum, file, nv - m->nv, 0)       \
  X(mjtNum, feature, nv - m->nv, 0)

// count pointers in mjData
static int getnptr_data(void) {
  int cnt = 0;

#define X(type, name, nr, nc) \
  if (!strcmp(#type, "mjtNum")) cnt++;

  MJDATA_POINTERS
#undef X

  return cnt;
}

// count pointers
static int getnptr_computed(void) {
  int cnt = 0;

#define X(type, name, nr, nc) \
  if (!strcmp(#type, "mjtNum")) cnt++;

  MJCOMPUTED_FIELDS
#undef X

  return cnt;
}

// key(std::string) : value(int) map
typedef struct _mjFeatureMap {
  std::string key;
  mjtObj item_type;
  int nr;
  int nc;
  int buffer_adr;
  mjtFEATURE_FIELD field;
} mjFeatureMap;

static int makeFeatureMapTable(const mjModel* m, mjFeatureMap* dataFields_map) {
  mjData* d = mj_makeData(m);

  int i = 0;
  // prepare variable sizes needed by xmacro
  int nv = m->nv, nemax = m->nemax, njmax = m->njmax;
#define X(type, name, _nr, _nc)                              \
  if (!strcmp(#type, "mjtNum") || !strcmp(#type, "mjtNum")) { \
    dataFields_map[i].key = #name;                           \
    dataFields_map[i].nr = m->_nr;                           \
    dataFields_map[i].nc = _nc;                              \
    if (!strcmp(#_nr, "nq") || !strcmp(#_nr, "nv"))          \
      dataFields_map[i].item_type = mjOBJ_JOINT;             \
    else if (!strcmp(#_nr, "nu"))                            \
      dataFields_map[i].item_type = mjOBJ_ACTUATOR;          \
    else if (!strcmp(#_nr, "nbody"))                         \
      dataFields_map[i].item_type = mjOBJ_BODY;              \
    else if (!strcmp(#_nr, "njnt"))                          \
      dataFields_map[i].item_type = mjOBJ_JOINT;             \
    else if (!strcmp(#_nr, "ngeom"))                         \
      dataFields_map[i].item_type = mjOBJ_GEOM;              \
    else if (!strcmp(#_nr, "nsite"))                         \
      dataFields_map[i].item_type = mjOBJ_SITE;              \
    else if (!strcmp(#_nr, "ntendon"))                       \
      dataFields_map[i].item_type = mjOBJ_TENDON;            \
    else if (!strcmp(#_nr, "nemax"))                         \
      dataFields_map[i].item_type = mjOBJ_EQUALITY;          \
    else                                                     \
      dataFields_map[i].item_type = mjOBJ_UNKNOWN;           \
    dataFields_map[i].field = mjFIELD_DATA;                  \
    i++;                                                     \
  }

  MJDATA_POINTERS
  MJCOMPUTED_FIELDS

  i = 0;
#undef X
#define X(type, name, _nr, _nc)                                                \
  if (!strcmp(#type, "mjtNum") || !strcmp(#type, "mjtNum")) {                   \
    dataFields_map[i].buffer_adr = (int)((mjtNum*)d->name - (mjtNum*)d->buffer); \
    i++;                                                                       \
  }

  MJDATA_POINTERS
#undef X
  int k = 0;
#define X(type, name, _nr, _nc)                  \
  dataFields_map[i].buffer_adr = 0;              \
  dataFields_map[i].field = (mjtFEATURE_FIELD)k; \
  i++;                                           \
  k++;

  MJCOMPUTED_FIELDS
#undef X
  if (k != mjFIELD_DATA)
    throw ttError(
        0, "something got messed up in the construction of dataFields_map");
  mj_deleteData(d);
  return i;
}

int mjCDatums::Compile(const mjModel* m) {
  int n_coefs;
  int n_refs;
  int n_entries;
  int n_items;
  mjDatum* datum;

  mjFeatureMap dataFields_map[200];
  int dataFields_sz = makeFeatureMapTable(m, dataFields_map);

  int itemsize;
  int entrysize;
  int buffer_adr_field;
  int adr = 0;
  int* padr = &adr;
  // find the identifier of the field:
  if (fieldname == "const") {
    if (refs.size() > 0 || coefs.size() != 1)
      throw ttError(
          this,
          "a const must have one scalar value in the coef and nothing else",
          fieldname.c_str());
    datum = new mjDatum;
    datums.push_back(datum);
    datum->field = mjFIELD_CONST;
    datum->coef = coefs[0];
    datum->ref = 0;
    n_items = 1;
    n_entries = 1;
  } else {
    // identify field:
    mjtByte success = 0;
    for (int i = 0; i < dataFields_sz; i++)
      if (dataFields_map[i].key == fieldname) {
        itemsize = dataFields_map[i].nr;
        entrysize = dataFields_map[i].nc;
        buffer_adr_field = dataFields_map[i].buffer_adr;
        itemtype = dataFields_map[i].item_type;
        field = dataFields_map[i].field;
        success = 1;
        break;
      }
    if (!success) throw ttError(this, "field %s not found", fieldname.c_str());

    // check for extra flags:
    if (fieldname == "cacc" || fieldname == "cfrc_int" ||
        fieldname == "cfrc_ext" || fieldname == "xacc" ||
        fieldname == "geom_xacc" || fieldname == "site_xacc" ||
        fieldname == "xacc_local" || fieldname == "geom_xacc_local" ||
        fieldname == "site_xacc_local")
      extra_flags |= extftRNEPOST;

    if (fieldname == "subcom" || fieldname == "subcomvel" ||
        fieldname == "momentum")
      extra_flags |= extftSUBTREE;

    if (fieldname == "sqrtminv") extra_flags |= extftSQRTMINV;

    if (!itemsize && !entrysize)  // the only fields without a specified
                                  // itemsize nor entrysize are feature and file
    {
      // feature or file
      if (fieldname == "feature") {
        // use!
        mjCFeature* pfeat = fc->FindFeature(itemnames);
        if (!pfeat)
          throw ttError(this,
                        "feature %s not found (could it be defined later? "
                        "order of definition matters here!)",
                        itemnames.c_str());
        items.push_back(pfeat->id);  // this is not the same as f_inx, which
                                     // will be written into pfeat->adr in
                                     // make_features
        entrysize = pfeat->size;
      } else if (fieldname == "file") {
        mjCDatafileColumn* pex = fc->FindDatafileColumn(itemnames);
        if (!pex)
          throw ttError(this,
                        "file column %s not found (could it be defined later? "
                        "order of definition matters here!)",
                        itemnames.c_str());
        items.push_back(pex->col);  // this is the offset where this particular
                                    // column starts, so it's good to go
        entrysize = pex->n;
      }

    }
    // find the item's index by its name, "all" is a special case:
    else if (!itemnames.length() || itemnames == "all") {
      for (int i = 0; i < itemsize; i++) items.push_back(i);
    } else {
      std::istringstream strm(itemnames);
      std::string token;
      while (!strm.eof()) {
        strm >> token;
        // first, try to parse it as a range:
        int ibuffer[2] = {0, 0};
        if (sscanf(token.c_str(), "%d:%d", ibuffer, ibuffer + 1) == 2) {
          for (int i = ibuffer[0]; i <= ibuffer[1]; i++) items.push_back(i);
        }
        // try to parse it as an int:
        else if (sscanf(token.c_str(), "%d", ibuffer) == 1) {
          if (itemtype > mjOBJ_UNKNOWN &&
              ibuffer[0] >= _getnumadr(m, itemtype, &padr)) {
            std::string err =
                "item number " + std::to_string((long long)ibuffer[0]) +
                " is larger than the number of objects of this type (" +
                std::to_string((long long)_getnumadr(m, itemtype, &padr)) + ")";
            throw ttError(this, err.c_str());
          }
          items.push_back(ibuffer[0]);
        } else {
          // else, let's hope it's a recognized name
          int id = mj_name2id(m, itemtype, token.c_str());
          if (id == -1) throw ttError(this, "item %s not found", token.c_str());

          if (itemtype == mjOBJ_JOINT) {
            if (fieldname == "qpos"){
              int jntsize;
              if (m->jnt_type[id] == mjJNT_FREE)
                jntsize = 7;
              else if (m->jnt_type[id] == mjJNT_BALL)
                jntsize = 4;
              else
                jntsize = 1;
              for (int k = 0; k < jntsize; k++) items.push_back(m->jnt_qposadr[id]+k);
            }
            else if (fieldname == "qvel" || fieldname == "qvel_next" ||
                     fieldname.substr(0, 5) == "qfrc_"){
              int jntsize;
              if (m->jnt_type[id] == mjJNT_FREE)
                jntsize = 6;
              else if (m->jnt_type[id] == mjJNT_BALL)
                jntsize = 3;
              else
                jntsize = 1;
              for (int k = 0; k < jntsize; k++) items.push_back(m->jnt_dofadr[id]+k);
            }
            else
              throw ttError(
                  this,
                  "joint based indexing allowed only for non-matrix q* fields");
          } else {
            items.push_back(id);
          }
        }
      }
    }

    // interpret entry (optional - make sure the entry type fits the field)
    if (!entrynames.length() || entrynames == "all") {
      for (int i = 0; i < entrysize; i++) entries.push_back(i);
    } else {
      std::istringstream strm(entrynames);
      std::string token;
      while (!strm.eof()) {
        strm >> token;
        success = 0;
        // first, try to parse token as a range:
        int ibuffer[2];
        if (sscanf(token.c_str(), "%d:%d", ibuffer, ibuffer + 1) == 2) {
          if (ibuffer[0] >= entrysize) {
            std::string err =
                "item number " + std::to_string((long long)ibuffer[0]) +
                " is larger than the number of objects of this type (" +
                std::to_string((long long)_getnumadr(m, itemtype, &padr)) + ")";
            throw ttError(this, err.c_str());
          }
          if (ibuffer[1] >= entrysize) {
            std::string err =
                "item number " + std::to_string((long long)ibuffer[1]) +
                " is larger than the number of objects of this type (" +
                std::to_string((long long)_getnumadr(m, itemtype, &padr)) + ")";
            throw ttError(this, err.c_str());
          }
          for (int i = ibuffer[0]; i <= ibuffer[1]; i++) entries.push_back(i);
        }
        // try to parse it as an int:
        else if (sscanf(token.c_str(), "%d", ibuffer) == 1) {
          if (ibuffer[0] >= entrysize) {
            std::string err =
                "item " + std::to_string((long long)ibuffer[0]) +
                " is larger than the number of objects of this type (" +
                std::to_string((long long)_getnumadr(m, itemtype, &padr)) + ")";
            throw ttError(this, err.c_str());
          }
          entries.push_back(ibuffer[0]);
        } else {
          // parse it as a std::string:
          for (int i = 0; i < entry_sz; i++)
            if (entry_map[i].key == token) {
              if (entrysize != entry_map[i].size)
                throw ttError(
                    this,
                    "entry size mismatch: entry name %s, got %d, expected %d",
                    token.c_str(), entrysize, entry_map[i].size);
              else
                entries.push_back(entry_map[i].value);
              success = 1;
              break;
            }

          if (!success)
            throw ttError(this, "entry %s not found", token.c_str());
        }
      }
    }

    // make datums, push into vector:

    n_coefs = (int)coefs.size();
    n_refs = (int)refs.size();
    n_entries = (int)entries.size();
    n_items = (int)items.size();

    // sanity checks:
    if (n_coefs > 1 && n_refs > 1 && n_coefs != n_refs)
      throw ttError(this,
                    "if no singleton expansion, then the number of coefs and "
                    "refs must match");

    int n_cr = mjMAX(n_coefs, n_refs);

    if (n_entries > 1 && n_items > 1 && n_cr > 1)
      throw ttError(this,
                    "if we have a matrix expansion of entries x items, coefs "
                    "and refs must be singletons at most (otherwise, how do "
                    "you know how to order them?)");
    if (!n_entries || !n_items)
      throw ttError(this,
                    "mjCDatums compilation error: how did we get here with no "
                    "items/entries?!");

    int n_datums = n_items * n_entries;

    if (n_cr > 1 && n_cr != n_datums)
      throw ttError(this,
                    "if no singleton expansion, the number of refs/coefs must "
                    "match the number of entries/items");

    for (int i = 0; i < n_items; i++)
      for (int j = 0; j < n_entries; j++) {
        datum = new mjDatum;
        datums.push_back(datum);
        datum->field = field;
        datum->item = itemtype;

        datum->entry = entries[j];
        datum->item = items[i];

        datum->buffer_adr =
            buffer_adr_field + datum->item * entrysize + datum->entry;

        int k = i * n_entries + j;
        if (n_coefs) {
          if (n_coefs > 1)
            datum->coef = coefs[k];
          else  // singleton expansion for coef
            datum->coef = coefs[0];
        } else  // default coef
          datum->coef = 1;

        if (n_refs) {
          if (n_refs > 1)
            datum->ref = refs[k];
          else  // singleton expansion for ref
            datum->ref = refs[0];
        } else  // default ref
          datum->ref = 0;
      }
  }
  return n_items * n_entries;
}

mjCFeature::mjCFeature(const mjModel* _m, mjCFC* _fc) {
  // set model pointer
  m = _m;
  fc = _fc;
  component = 0;
  adr = -1;
  size = 0;
  name = "feature";
}
mjCFeature::~mjCFeature() {
  if (component) delete component;
}

void mjCFeature::Compile(const mjModel* m) {
  size = component->Compile(m);
  // adr will be filled when the low-level representation is compiled
}

mjCOperator::mjCOperator(const mjModel* _m, mjCFC* _fc) {
  m = _m;
  fc = _fc;

  in1 = 0;
  in1_adr = -1;
  in1_size = 0;

  in2 = 0;
  in2_adr = -1;
  in2_size = 0;

  fun = mj_OP_GET_DATUM;
  out_adr = -1;
  out_size = 0;

  lossNorm = mjNL_GENERIC;

  coef = 1;
  ref = 0;

  params.clear();
  funname.clear();
  name = "operator";
}

mjCOperator::~mjCOperator() {
  if (in1) delete in1;
  if (in2) delete in2;

  params.clear();
  funname.clear();
}

extern mjtFeatureOperation operatorList[NUM_OPERATORS];
extern mjtNorm funList[NUM_NORM];

int mjCOperator::Compile(const mjModel* m) {
  in1_size = in1->Compile(m);

  if (in2)
    in2_size = in2->Compile(m);
  else  // maybe there's only one operand!
    in2_size = 0;

  int expected_nparams = -1;
  // expected inputs dimension (0 means any)
  int expected_in1_size = 0;
  int expected_in2_size = 0;
  mjtByte isnorm = 0;
  std::string NLname;

  // process "loss" and "norm" types
  if (lossNorm) {
    NLname = funname;
    if (lossNorm == mjNL_LOSS)
      funname = "loss";
    else
      funname = "norm";

    for (int i = 0; i < NUM_NORM; i++)
      if (funList[i].name == NLname) {
        isnorm = 1;
        expected_nparams = funList[i].nparam;
        // test before param list is modified:
        if (expected_nparams != params.size())
          throw ttError(this, "norm/loss %s requires %d params, got %d",
                        NLname.c_str(), expected_nparams, (int)params.size());
        // insert norm identifier to the top of params
        if (params.size()) {
          params.push_back(params.back());
          for (int j = 0; j < params.size() - 1; j++) params[j + 1] = params[j];
          params[0] = funList[i].type;
        } else
          params.push_back(funList[i].type);
        break;
      }
    if (!isnorm)  // failed to identify norm/loss by name, produce a detailed
                  // error message with list of existing norms/losses
    {
      std::string optypes;
      for (int i = 0; i < NUM_NORM; i++)
        optypes = optypes + "\n" + funList[i].name + " (" +
                  std::to_string((long long)(int)funList[i].nparam) + ")";
      std::string err = "norm/loss '" + NLname +
                        "' not found.\nname (nparams)\n=========" + optypes;
      throw ttError(this, err.c_str());
    }
  }
  mjtByte isoperator = 0;
  for (int i = 0; i < NUM_OPERATORS; i++)
    if (operatorList[i].name == funname) {
      fun = (mjtOperatorType)i;
      expected_nparams = operatorList[i].nparam;
      expected_in1_size = operatorList[i].indim1;
      expected_in2_size = operatorList[i].indim2;
      isoperator = 1;
      break;
    }

  if (!isoperator)  // failed to identify function by name, produce a detailed
                    // error message with list of existing functions
  {
    std::string optypes;
    for (int i = 0; i < NUM_OPERATORS; i++)
      optypes = optypes + "\n" + operatorList[i].name;
    std::string err =
        "operator '" + funname + "' not found.\n=========" + optypes;
    throw ttError(this, err.c_str());
  }

  if (!isnorm && expected_nparams != params.size())
    throw ttError(this, "operator %s requires %d params, got %d",
                  funname.c_str(), expected_nparams, (int)params.size());
  if (expected_in1_size && expected_in1_size != in1_size)
    throw ttError(this,
                  "the first input of operator %s must be of size %d, got %d",
                  funname.c_str(), expected_in1_size, in1_size);
  if (expected_in2_size && in2_size && expected_in2_size != in2_size)
    throw ttError(this,
                  "the second input of operator %s must be of size %d, got %d",
                  funname.c_str(), expected_in2_size, in2_size);

  // a dummy call to the operator function in order to see how long is its out:
  std::unique_ptr<mjtNum[]> temp(new mjtNum[3000]);
  mju_zero(temp.get(), 3000);
  out_size = mjf_operator(temp.get(), temp.get(), in1_size, temp.get(),
                          in2_size, fun, temp.get());
  return out_size;
}

// adding a single data feature and multiple datums to residual.
int add_dat(mjResidual* res, mjCDatums* dat, int f_inx, int initIndexInDatums) {
  res->features[f_inx].fun = (int)mj_OP_GET_DATUM;
  res->features[f_inx].ref = 0;
  res->features[f_inx].coef = 1;

  if (initIndexInDatums + (int)dat->datums.size() > MAX_NF)
    mju_error("too many features");
  // copy dat's datums into res->datums
  res->features[f_inx].datums_adr = initIndexInDatums;
  res->features[f_inx].datums_size = (int)dat->datums.size();
  for (int j = 0; j < res->features[f_inx].datums_size; j++)
    res->datums[initIndexInDatums + j] = *dat->datums[j];

  res->extra_flags |= dat->extra_flags;

  res->nf += res->features[f_inx].datums_size;
  res->num_features++;
  return res->features[f_inx].datums_size;
}

int add_op(mjResidual* res, mjCOperator* op, int* pd_index, int* pf_index,
           std::vector<mjCFeature*> vf) {
  if (typeid(*op->in1) == typeid(mjCOperator)) {
    op->in1_size = add_op(res, (mjCOperator*)op->in1, pd_index, pf_index, vf);
    op->in1_adr = (*pf_index) - 1;
  } else {
    op->in1_size = add_dat(res, (mjCDatums*)op->in1, *pf_index, *pd_index);
    op->in1_adr = *pf_index;
    *pd_index += op->in1_size;
    (*pf_index)++;
  }

  if (op->in2) {
    if (typeid(*op->in2) == typeid(mjCOperator)) {
      op->in2_size = add_op(res, (mjCOperator*)op->in2, pd_index, pf_index, vf);
      op->in2_adr = (*pf_index) - 1;
    } else {
      op->in2_size = add_dat(res, (mjCDatums*)op->in2, *pf_index, *pd_index);
      op->in2_adr = *pf_index;
      *pd_index += op->in2_size;
      (*pf_index)++;
    }
  } else {
    op->in2_adr = 0;
    op->in2_size = 0;
  }

  // at this stage, both children are properly processed
  int f_index = *pf_index;
  op->out_adr = f_index;
  res->features[f_index].datums_adr = -1;
  res->features[f_index].datums_size = 0;
  res->features[f_index].fun = op->fun;
  res->features[f_index].ref = op->ref;
  res->features[f_index].coef = op->coef;
  res->features[f_index].in1_adr = op->in1_adr;
  res->features[f_index].in1_size = op->in1_size;
  res->features[f_index].in2_adr = op->in2_adr;
  res->features[f_index].in2_size = op->in2_size;

#ifdef ttVERBOSE
  if (res->features[f_index].in1_adr < -1 ||
      res->features[f_index].in2_adr < -1)
    mju_warning_i("(add_op) bad indexes in feature %d\n", f_index);
#endif
  for (int i = 0; i < op->params.size(); i++)
    res->features[f_index].params[i] = op->params[i];

  (*pf_index)++;
  res->nf += op->out_size;
  res->num_features++;
  return op->out_size;
}

void interpret_features(mjResidual* res, std::vector<mjCFeature*> vf) {
  int i;
  int f_inx = 0, d_inx = 0;
  mjCFeature* f;
  res->nf = 0;
  res->num_features = 0;
  for (i = 0; i < vf.size(); i++) {
    f = vf[i];
    if (typeid(*f->component) == typeid(mjCDatums)) {
      d_inx = d_inx + add_dat(res, (mjCDatums*)f->component, f_inx, d_inx);
      f_inx++;
    } else
      add_op(res, (mjCOperator*)f->component, &d_inx, &f_inx, vf);

    f->adr = f_inx - 1;
  }

  // loop over datums, if anyone was looking for a feature field, convert their
  // item from index in vf to adr:
  for (i = 0; i < d_inx; i++)
    if (res->datums[i].field == mjFIELD_FEATURE)
      res->datums[i].item = vf[res->datums[i].item]->adr;
}

mjCTweak::mjCTweak(const mjModel* _m, mjCFC* _fc) {
  m = _m;
  fc = _fc;
}

void mjCTweak::Compile() {
  int i;
  mjtObj itemtype;

  // find the identifier of the field:
  mjtByte success = 0;
  for (i = 0; i < modelFields_sz; i++)
    if (modelFields_map[i].key == fieldname) {
      field = modelFields_map[i].field;
      itemtype = modelFields_map[i].item_type;
      success = 1;
      break;
    }
  if (!success) throw ttError(this, "field %s not found", fieldname.c_str());

  // parse item by type:
  // first, try to parse it as an int:
  int buffer[1] = {0};
  if (sscanf(itemname.c_str(), "%d", buffer) == 1) {
    item = buffer[0];
  } else {  // hopefully, it's a proper name
    if (field == mjMF_FEATURE) {
      ttBase* itemptr;
      itemptr = fc->FindFeature(itemname);
      item = itemptr->id;  // this is the index in the vector, not the f_inx
    } else
      item = mj_name2id(m, itemtype, itemname.c_str());

    if (item < 0) throw ttError(this, "item %s not found", itemname.c_str());
  }

  switch (field) {
    case mjMF_FEATURE:
      item = fc->features[item]->adr;  // switch from index in model to f_inx
      break;
    default:
      throw ttError(this,
                    "mjCTweak::Compile : so far only features are supported "
                    "for tweaks (got field %s).",
                    fieldname.c_str());
  }

  // parse entry

  success = 0;
  for (i = 0; i < entry_sz; i++)
    if (entry_map[i].key == entryname) {
      entry = entry_map[i].value;
      success = 1;
      break;
    }

  if (!success) throw ttError(this, "entry %s not found", entryname.c_str());
}

mjCDatafileColumn::mjCDatafileColumn(const mjModel* _m, mjCFC*) { row_offset = 0; }

mjCCost_term::~mjCCost_term() {
  params.clear();
  f_inx = -1;
}

mjCCost_term::mjCCost_term(const mjModel* _m, mjCFC* _fc) {
  m = _m;
  fc = _fc;
  coef[0] = 0;
  coef[1] = mju_sqrt(-1.0);
  params.clear();
}

mjCTransition::~mjCTransition() { params.clear(); }
mjCTransition::mjCTransition(const mjModel* _m, mjCFC* _fc) {
  m = _m;
  fc = _fc;
  params.clear();
  adr = -1;
}

// similar to transition::compile!!!
void mjCCost_term::Compile(int _id) {
  id = _id;
  // interpret normname:
  int nparams, normdim;
  mjtByte success = 0;
  // first, try to parse it as a number which is the tag of a geom
  int buffer[1] = {0};
  if (sscanf(normname.c_str(), "%d", buffer) == 1) {
    // find a geom with the tag buffer[0]:
  } else {
    for (int i = 0; i < NUM_NORM; i++)
      if (funList[i].name == normname) {
        normid = (mjtNormType)i;
        nparams = funList[i].nparam;
        normdim = funList[i].dim;
        isloss = funList[i].isloss > 0;
        success = 1;
        break;
      }
    if (!success) {
      std::string optypes;
      for (int i = 0; i < NUM_NORM; i++)
        optypes = optypes + "\n" + funList[i].name + " (" +
                  std::to_string((long double)funList[i].nparam) + ")";
      std::string err = "norm '" + normname +
                        "' not found.\nname (nparams):\n=========" + optypes;
      throw ttError(this, err.c_str());
    }
    if (nparams != params.size())
      throw ttError(this, "norm %s requires %d params, got %d",
                    normname.c_str(), nparams, (int)params.size());
  }

  if (!name.length()) name = featurename;

  // interpret featurename:
  mjCFeature* feature = fc->FindFeature(featurename);
  if (!feature)
    throw ttError(this, "feature name %s not found", featurename.c_str());
  f_inx = feature->adr;
  // r_adr will be filled in later
  nr = feature->size;

  if (normdim && normdim != nr)
    throw ttError(this, "norm %s requires %d features, got %d",
                  normname.c_str(), normdim, nr);
  if (name.length() > 20) name = name.substr(0, 20);
}

// similar to cost_term::compile!!!
void mjCTransition::Compile(bool dataTransition) {
  // interpret normname:
  int nparams, normdim;
  // interpret feature_set_name:
  if (feature_name == "timeout") {
    adr = -1;
    f_inx = -1;
    nr = 0;
  } else {
    // interpret feature_name:
    mjCFeature* feature = (mjCFeature*)fc->FindFeature(feature_name);
    if (!feature)
      throw ttError(this, "feature name %s not found", feature_name.c_str());
    f_inx = feature->adr;
    nr = feature->size;

    mjtByte success = 0;
    for (int i = 0; i < NUM_NORM; i++)
      if (funList[i].name == normname) {
        normid = (mjtNormType)i;
        nparams = funList[i].nparam;
        normdim = funList[i].dim;
        isloss = funList[i].isloss > 0;
        success = 1;
        break;
      }
    if (!success) throw ttError(this, "norm %s not found", normname.c_str());
    if (nparams != params.size())
      throw ttError(this, "norm %s requires %d params, got %d",
                    normname.c_str(), nparams, (int)params.size());
    if (normdim && normdim != nr)
      throw ttError(this, "norm %s requires %d features, got %d",
                    normname.c_str(), normdim, nr);
  }

  if (!dataTransition) {
    mjCCost* to_cost = fc->FindCost(to_name);
    if (!to_cost) throw ttError(this, "to-cost %s not found", to_name.c_str());
    to_id = to_cost->id;
  }
}

mjCAlter::mjCAlter(const mjModel* _m) {
  m = _m;
  adr = -1;
}

void mjCAlter::Compile(const mjModel* m) {
  throw ttError(this, "alters are broken!");

  /*
          int i, expected_nr;
          mjFeatureMap dataFields_map[200];
          int dataFields_sz = makeFeatureMapTable(m, dataFields_map);

          // find the identifier of the field:
          mjtByte success = 0;
          for( i=0; i<modelFields_sz; i++ )
                  if( modelFields_map[i].key == fieldname )
                  {
                          field = modelFields_map[i].field;
                          itemtype = modelFields_map[i].item_type;
                          int buffer_sizes[NUM_FIELDS][3];
                          mj_DataBufferSizes(m, buffer_sizes);
                          expected_nr = buffer_sizes[field][1];
                          success = 1;
                          break;
                  }
                  if( !success )
                          throw ttError(this,
                          "field %s not found", fieldname.c_str());

                  //parse item by type:
                  //first, try to parse it as an int:
                  int buffer[1] = {0};
                  if( sscanf(itemname.c_str(), "%d", buffer) == 1 )
                  {
                          item=buffer[0];
                  }
                  else
                  { //hopefully, it's a proper name
                          mjCBase *itemptr = model->FindObject(itemtype,
     itemname);
                          if( !itemptr )
                                  throw ttError(this, "item %s not found",
     itemname.c_str());
                          item = itemptr->id;
                  }

                  switch( field )
                  {
                  case mjMF_GEOM_POS:
                          buffer_offset = (char*)(m->geom_pos+3*item) -
     (char*)m->buffer;
                          break;
                  case mjMF_QPOS_SPRING:
                          buffer_offset =
     (char*)(m->qpos_spring+m->jnt_qposadr[item]) - (char*)m->buffer;
                          break;
                  case mjMF_STIFFNESS:
                          buffer_offset = (char*)(m->jnt_stiffness+item) -
     (char*)m->buffer;
                          break;
                  case mjMF_TENDON_SPRING:
                          buffer_offset = (char*)(m->tendon_lengthspring+item) -
     (char*)m->buffer;
                          break;
                  case mjMF_TENDON_STIFFNESS:
                          buffer_offset = (char*)(m->tendon_stiffness+item) -
     (char*)m->buffer;
                          break;
                  }



                  //interpret feature_set_name:
                  mjCFeature* feature = (mjCFeature
     *)model->FindObject(mjOBJ_FEATURE,feature_name);
                  if( !feature )
                          throw ttError(this, "feature name %s not found",
     feature_name.c_str());
                  f_inx = feature->adr;
                  nr = feature->size;

                  if( expected_nr!=nr )
                          throw ttError(this, "field %s requires feature of size
     %d, got %d", fieldname.c_str(), expected_nr, nr);
                          */
}

mjCCost::mjCCost(const mjModel* _m, mjCFC* _fc) {
  // model=_model; // what for?!
  terms.clear();
  transitions.clear();
  mj_defaultOption(&opt);
  // opt.disableflags
}

void mjCCost::Compile_terms() {
  for (int i = 0; i < terms.size(); i++) terms[i]->Compile(i);
}

mjCCost::~mjCCost() {
  for (int i = 0; i < terms.size(); i++) delete terms[i];
  terms.clear();
  for (int i = 0; i < transitions.size(); i++) delete transitions[i];
  transitions.clear();
}
