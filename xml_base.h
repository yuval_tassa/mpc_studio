#ifndef _TT_MJ_XML_BASE_H_
#define _TT_MJ_XML_BASE_H_

#include "mujoco.h"
#include "tinystr.h"
#include "tinyxml.h"
#include "stdio.h"
#include "float.h"

#include <vector>
#include <string>
#include <iostream>
#include <sstream>


// max number of attribute fields in schema (plus 3)
#define mjXATTRNUM 33

#include "util.h"

//----------------------------- Helper classes ---------------------------------

// XML Error info
class mjXError {
 public:
  mjXError(const TiXmlElement* elem = 0, const char* msg = 0,
           const char* str = 0, int pos = 0);

  char message[500];  // error message
};

// Custom XML file validation
class mjXSchema {
 public:
  mjXSchema(const char* schema[][mjXATTRNUM], int nrow,  // constructor
            bool checkptr = true);
  ~mjXSchema();                             // destructor
  std::string GetError(void);               // return error
  void Print(FILE* fp, int level = 0);      // print schema
  void PrintHTML(FILE* fp, int level = 0);  // print schema as HTML table

  TiXmlElement* Check(TiXmlElement* elem);  // validator

 private:
  std::string name;               // element name
  char type;                      // element type: '?', '!', '*', 'R'
  std::vector<std::string> attr;  // allowed attributes
  std::vector<mjXSchema*> child;  // allowed child elements

  int refcnt;         // refcount used for validation
  std::string error;  // error from constructor or Check
};

// key(std::string) : value(int) map
typedef struct _mjMap {
  std::string key;
  int value;
} mjMap;

//----------------------------- Base XML class
//------------------------------------------

class mjXBase {
 public:
  mjXBase();

  // parse: implemented in derived parser classes
  void Parse(TiXmlElement* root, std::string filename){};

  //---------------------------- static helper functions

  // find key in map, return value (-1: not found)
  static int FindKey(const mjMap* map, int mapsz, std::string key);

  // find value in map, return key ("": not found)
  static std::string FindValue(const mjMap* map, int mapsz, int value);

  // read double array from attribute, return number read
  static int ReadAttr(TiXmlElement* elem, const char* attr, const int len,
                      double* data, std::string& text, bool required = false,
                      bool exact = true);

  // read float array from attribute, return number read
  static int ReadAttr(TiXmlElement* elem, const char* attr, const int len,
                      float* data, std::string& text, bool required = false,
                      bool exact = true);

  // read text attribute
  static bool ReadAttrTxt(TiXmlElement* elem, const char* attr,
                          std::string& text, bool required = false);

  // read int attribute
  static bool ReadAttrInt(TiXmlElement* elem, const char* attr, int* data,
                          bool required = false);

  // find subelement with given name, make sure it is unique
  static TiXmlElement* FindSubElem(TiXmlElement* elem, std::string name,
                                   bool required = false);

  // find attribute, translate key, return int value
  static bool MapValue(TiXmlElement* elem, const char* attr, int* data,
                       const mjMap* map, int mapSz, bool required = false);

  // is directory path absolute
  static bool IsAbsPath(std::string path);

  // get directory path of file
  static std::string GetFileDir(std::string filename);

  // get directory path of file
  static std::string FullName(std::string filedir, std::string meshdir,
                              std::string filename);

  // Tom's functions
  int ReadContent(TiXmlElement* elem, const int len, double* data,
                  std::string& text, bool required, bool exact);
  int ReadSubElem(TiXmlElement* elem, std::string name, const int len,
                  double* data, std::string& text, bool required, bool exact);
  bool ReadSubElemTxt(TiXmlElement* elem, const std::string name,
                      std::string& text, bool required);

  //---------------------------- helper write functions

  // write attribute- double
  void WriteAttr(TiXmlElement* elem, std::string name, int n, double* data,
                 const double* def = 0);

  // write attribute- float
  void WriteAttr(TiXmlElement* elem, std::string name, int n, float* data,
                 const float* def = 0);

  // write attribute- std::string
  void WriteAttrTxt(TiXmlElement* elem, std::string name, std::string value);

  // write attribute- single int
  void WriteAttrInt(TiXmlElement* elem, std::string name, int data,
                    int def = -12345);

  // write attribute- keyword
  void WriteAttrKey(TiXmlElement* elem, std::string name, const mjMap* map,
                    int mapsz, int data, int def = -12345);

  const mjModel* m;  // internally-allocated mjModel object
};

#endif  // _TT_MJ_XML_BASE_H_
