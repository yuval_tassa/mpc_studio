#include "xml_base.h"

//--------------------- class mjXError implementation --------------------------

mjXError::mjXError(const TiXmlElement* elem, const char* msg, const char* str,
                   int pos) {
  char temp[500];

  // construct error message
  sprintf(message, "XML Error");
  if (msg) {
    sprintf(temp, msg, str, pos);
    strcat(message, ": ");
    strcat(message, temp);
  }

  // append element, line and column numbers
  if (elem) {
    sprintf(temp, "\nElement '%s', line %d, column %d\n", elem->Value(),
            elem->Row(), elem->Column());

    strcat(message, temp);
  }
}

//--------------------- class mjXSchema implementation -------------------------

// constructor
mjXSchema::mjXSchema(const char* schema[][mjXATTRNUM], int nrow,
                     bool checkptr) {
  // clear fields
  name.clear();
  type = '?';
  child.clear();
  attr.clear();
  error.clear();

  // checks nrow and first element
  if (nrow < 1) {
    error = "number of rows must be positive";
    return;
  }
  if (schema[0][0][0] == '<' || schema[0][0][0] == '>') {
    error = "expected element, found bracket";
    return;
  }

  // check entire schema for null pointers
  if (checkptr) {
    char msg[100];

    for (int i = 0; i < nrow; i++) {
      // base pointers
      if (!schema[i] || !schema[i][0]) {
        sprintf(msg, "null pointer found in row %d", i);
        error = msg;
        return;
      }

      // detect element
      if (schema[i][0][0] != '<' && schema[i][0][0] != '>') {
        // first 3 pointers required
        if (!schema[i][1] || !schema[i][2]) {
          sprintf(msg, "null pointer in row %d, element %s", i, schema[i][0]);
          error = msg;
          return;
        }

        // check type
        if (schema[i][1][0] != '!' && schema[i][1][0] != '?' &&
            schema[i][1][0] != '*' && schema[i][1][0] != 'R') {
          sprintf(msg, "invalid type in row %d, element %s", i, schema[i][0]);
          error = msg;
          return;
        }

        // number of attributes
        int nattr = atoi(schema[i][2]);
        if (nattr < 0 || nattr > mjXATTRNUM - 3) {
          sprintf(msg, "invalid number of attributes in row %d, element %s", i,
                  schema[i][0]);
          error = msg;
          return;
        }

        // attribute pointers
        for (int j = 0; j < nattr; j++)
          if (!schema[i][3 + j]) {
            sprintf(msg, "null attribute %d in row %d, element %s", j, i,
                    schema[i][0]);
            error = msg;
            return;
          }
      }
    }
  }

  // set name and type
  name = schema[0][0];
  type = schema[0][1][0];

  // set attributes
  int nattr = atoi(schema[0][2]);
  for (int i = 0; i < nattr; i++) attr.push_back(schema[0][3 + i]);

  // process sub-elements of complex element
  if (nrow > 1) {
    // check for bracketed block
    if (schema[1][0][0] != '<' || schema[nrow - 1][0][0] != '>') {
      error = "expected brackets after complex element";
      return;
    }

    // parse block into simple and complex elements, create children
    int start = 2;
    while (start < nrow - 1) {
      int end = start;

      // look for bracketed block at start+1
      if (schema[start + 1][0][0] == '<') {
        // look for corresponding closing bracket
        int cnt = 0;
        while (end <= nrow - 1) {
          if (schema[end][0][0] == '<')
            cnt++;
          else if (schema[end][0][0] == '>') {
            cnt--;
            if (cnt == 0) break;
          }

          end++;
        }

        // closing bracket not found
        if (end > nrow - 1) {
          error = "matching closing bracket not found";
          return;
        }
      }

      // add element, check for error
      mjXSchema* elem = new mjXSchema(schema + start, end - start + 1, false);
      child.push_back(elem);
      if (!elem->error.empty()) {
        error = elem->error;
        return;
      }

      // proceed with next subelement
      start = end + 1;
    }
  }
}

// destructor
mjXSchema::~mjXSchema() {
  // delete children recursively
  for (unsigned int i = 0; i < child.size(); i++) delete child[i];

  // clear fields
  child.clear();
  attr.clear();
  error.clear();
}

// get pointer to error message
std::string mjXSchema::GetError(void) { return error; }

#ifdef _MEX
#include "mex.h"
#define fprintf(file, ...) mexPrintf(__VA_ARGS__)
#endif

// print spaces
static void printspace(FILE* fp, int n) {
  for (int i = 0; i < n; i++) fprintf(fp, " ");
}

// print schema as tree
void mjXSchema::Print(FILE* fp, int level) {
  int i;

  // space, name, type
  printspace(fp, 3 * level);
  fprintf(fp, "%s (%c)", name.c_str(), type);
  int baselen = 3 * level + (int)name.size() + 4;
  if (baselen < 30) printspace(fp, 30 - baselen);

  // attributes
  int cnt = mjMAX(30, baselen);
  for (i = 0; i < (int)attr.size(); i++) {
    if (cnt > 60) {
      fprintf(fp, "\n");
      printspace(fp, (cnt = mjMAX(30, baselen)));
    }

    fprintf(fp, "%s ", attr[i].c_str());
    cnt += (int)attr[i].size() + 1;
  }
  fprintf(fp, "\n");

  // children
  for (i = 0; i < (int)child.size(); i++) child[i]->Print(fp, level + 1);
}

// print schema as HTML table
void mjXSchema::PrintHTML(FILE* fp, int level) {
  int i;

  // open table
  if (level == 0) fprintf(fp, "<table>\n");

  // name
  fprintf(fp, "<tr>\n\t<td style=\"padding-left:%d\" class=\"el\">%s</td>\n",
          5 + 10 * level, name.c_str());

  // type
  fprintf(fp, "\t<td class=\"ty\">%c</td>\n", type);

  // attributes
  fprintf(fp, "\t<td class=\"at\">");
  if (attr.size())
    for (i = 0; i < (int)attr.size(); i++) fprintf(fp, "%s ", attr[i].c_str());
  else
    fprintf(fp, "<span style=\"color:black\"><i>no attributes</i></span>");
  fprintf(fp, "</td>\n</tr>\n");

  // children
  for (i = 0; i < (int)child.size(); i++) child[i]->PrintHTML(fp, level + 1);

  // close table
  if (level == 0) fprintf(fp, "</table>\n");
}

#ifdef _MEX
#undef fprintf
#endif

// validator
TiXmlElement* mjXSchema::Check(TiXmlElement* elem) {
  int i;
  bool missing;
  char msg[100];
  TiXmlElement *bad, *sub;

  error.clear();
  if (!elem) return 0;  // SHOULD NOT OCCUR

  // check name (already done by parent, but hard to avoid)
  if (name != elem->Value()) {
    error = "unrecognized element";
    return elem;
  }

  // check attributes
  TiXmlAttribute* attribute = elem->FirstAttribute();
  while (attribute) {
    missing = true;
    for (i = 0; i < (int)attr.size(); i++)
      if (attr[i] == attribute->Name()) {
        missing = false;
        break;
      }
    if (missing) {
      error =
          "unrecognized attribute: '" + std::string(attribute->Name()) + "'";
      return elem;
    }

    // next attribute
    attribute = attribute->Next();
  }

  // handle recursion
  if (type == 'R') {
    // loop over sub-elements with same name
    sub = elem->FirstChildElement(elem->Value());
    while (sub) {
      // check sub-tree
      if ((bad = Check(sub))) return bad;

      // advance to next sub-element with same name
      sub = sub->NextSiblingElement(elem->Value());
    }
  }

  // clear reference counts
  for (i = 0; i < (int)child.size(); i++) child[i]->refcnt = 0;

  // check sub-elements, update refcnt
  sub = elem->FirstChildElement();
  while (sub) {
    // find in child array, update refcnt
    missing = true;
    for (i = 0; i < (int)child.size(); i++)
      if (child[i]->name == sub->Value()) {
        // check sub-tree
        if ((bad = child[i]->Check(sub))) {
          error = child[i]->error;
          return bad;
        }

        // mark found
        missing = false;
        child[i]->refcnt++;
        break;
      }

    // missing, unless recursive
    if (missing && (type != 'R' || name != sub->Value())) {
      error = "unrecognized element";
      return sub;
    }

    // advance to next sub-element
    sub = sub->NextSiblingElement();
  }

  // enforce sub-element types
  msg[0] = 0;
  for (i = 0; i < (int)child.size(); i++) switch (child[i]->type) {
      case '!':
        if (child[i]->refcnt != 1)
          sprintf(msg, "required sub-element '%s' found %d time(s)",
                  child[i]->name.c_str(), child[i]->refcnt);
        break;

      case '?':
        if (child[i]->refcnt > 1)
          sprintf(msg, "unique sub-element '%s' found %d time(s)",
                  child[i]->name.c_str(), child[i]->refcnt);
        break;

      default:
        break;
    }

  // handle error
  if (msg[0]) {
    error = msg;
    return elem;
  } else
    return 0;
}

//----------------------- Base class, helper functions
//----------------------------------

// base constructor
mjXBase::mjXBase() { m = 0; }

// find string in map, return corresponding integer (-1: not found)
int mjXBase::FindKey(const mjMap* map, int mapsz, std::string key) {
  for (int i = 0; i < mapsz; i++)
    if (map[i].key == key) return map[i].value;

  return -1;
}

// find integer in map, return corresponding string ("": not found)
std::string mjXBase::FindValue(const mjMap* map, int mapsz, int value) {
  for (int i = 0; i < mapsz; i++)
    if (map[i].value == value) return map[i].key;

  return "";
}

// read attribute "attr" of element "elem"
//  "len" is the number of floats or doubles to be read
//  the content is returned in "text", the numeric data in "data"
//  return true if attribute found, false if not found and not required

int mjXBase::ReadAttr(TiXmlElement* elem, const char* attr, const int len,
                      double* data, std::string& text, bool required,
                      bool exact) {
  const char* pstr = elem->Attribute(attr);

  // check if attribute exists
  if (!pstr) {
    if (required)
      throw mjXError(elem, "required attribute missing: '%s'", attr);
    else
      return 0;
  }

  // convert to string
  text = std::string(pstr);

  // get input stream
  std::istringstream strm(text);

  // read numbers
  int i;
  for (i = 0; i < len; i++) {
    strm >> data[i];
    if (strm.eof()) {
      i++;
      break;
    } else if (strm.bad())
      throw mjXError(elem, "problem reading attribute '%s'", attr);
  }

  // determine available length
  int available = i;
  if (strm.good()) {
    double dummy;
    strm >> dummy;
    if (!strm.bad() && !strm.fail()) available++;
  }

  // check
  if (exact && available < len)
    throw mjXError(elem, "attribute '%s' does not have enough data", attr);
  if (available > len)
    throw mjXError(elem, "attribute '%s' has too much data", attr);

  return i;
}

// float version
int mjXBase::ReadAttr(TiXmlElement* elem, const char* attr, const int len,
                      float* data, std::string& text, bool required,
                      bool exact) {
  const char* pstr = elem->Attribute(attr);

  // check if attribute exists
  if (!pstr) {
    if (required)
      throw mjXError(elem, "required attribute missing: '%s'", attr);
    else
      return 0;
  }

  // convert to string
  text = std::string(pstr);

  // get input stream
  std::istringstream strm(text);

  // read numbers
  int i;
  for (i = 0; i < len; i++) {
    strm >> data[i];
    if (strm.eof()) {
      i++;
      break;
    } else if (strm.bad())
      throw mjXError(elem, "problem reading attribute '%s'", attr);
  }

  // determine available length
  int available = i;
  if (strm.good()) {
    float dummy;
    strm >> dummy;
    if (!strm.bad() && !strm.fail()) available++;
  }

  // check
  if (exact && available < len)
    throw mjXError(elem, "attribute '%s' does not have enough data", attr);
  if (available > len)
    throw mjXError(elem, "attribute '%s' has too much data", attr);

  return i;
}

// read text field
bool mjXBase::ReadAttrTxt(TiXmlElement* elem, const char* attr,
                          std::string& text, bool required) {
  const char* pstr = elem->Attribute(attr);

  // check if attribute exists
  if (!pstr) {
    if (required)
      throw mjXError(elem, "required attribute missing: '%s'", attr);
    else
      return false;
  }

  // read text
  text = std::string(pstr);

  return true;
}

// read single int
bool mjXBase::ReadAttrInt(TiXmlElement* elem, const char* attr, int* data,
                          bool required) {
  const char* pstr = elem->Attribute(attr);

  // check if attribute exists
  if (!pstr) {
    if (required)
      throw mjXError(elem, "required attribute missing: '%s'", attr);
    else
      return false;
  }

  // convert to int, check
  int buffer[2] = {0, 0};
  if (sscanf(pstr, "%d", buffer) != 1)
    throw mjXError(elem, "single int expected in attribute %s", attr);

  // copy data
  *data = buffer[0];
  return true;
}

// find subelement with given name, make sure it is unique
TiXmlElement* mjXBase::FindSubElem(TiXmlElement* elem, std::string name,
                                   bool required) {
  TiXmlElement* subelem = 0;

  TiXmlElement* iter = elem->FirstChildElement();
  while (iter) {
    // identify elements with given name
    if (name == iter->Value()) {
      // make sure name is not repeated
      if (subelem)
        throw mjXError(subelem, "repeated element: '%s'", name.c_str());

      // save found element
      subelem = iter;
    }

    // advance to next element
    iter = iter->NextSiblingElement();
  }

  if (required && !subelem)
    throw mjXError(elem, "missing element: '%s'", name.c_str());

  return subelem;
}

// find attribute, translate key, return int value
bool mjXBase::MapValue(TiXmlElement* elem, const char* attr, int* data,
                       const mjMap* map, int mapSz, bool required) {
  // get attribute text
  std::string text;
  if (!ReadAttrTxt(elem, attr, text, required)) return false;

  // find keyword in map
  int value = FindKey(map, mapSz, text);
  if (value < 0) throw mjXError(elem, "invalid keyword: '%s'", text.c_str());

  // copy
  *data = value;
  return true;
}

// Tom's function
int mjXBase::ReadContent(TiXmlElement* elem, const int len, double* data,
                         std::string& text, bool required, bool exact) {
  const char* pstr = elem->GetText();

  // check if attribute exists
  if (!pstr) {
    if (required)
      throw mjXError(elem, "required attribute missing: '%s'", pstr);
    else
      return 0;
  }

  // convert to string
  text = std::string(pstr);

  // get input stream
  std::istringstream strm(text);

  // read numbers
  int i;
  for (i = 0; i < len; i++) {
    strm >> data[i];
    if (strm.eof()) {
      i++;
      break;
    } else if (strm.bad())
      throw mjXError(elem, "problem reading content '%s'",
                     elem->GetText());  // text.c_str());
  }

  // determine available length
  int available = i;
  if (strm.good()) {
    double dummy;
    strm >> dummy;
    if (!strm.bad() && !strm.fail()) available++;
  }

  // check
  if (exact && available < len)
    throw mjXError(elem, "content '%s' does not have enough data",
                   text.c_str());
  if (exact && available > len)
    throw mjXError(elem, "content '%s' has too much data", text.c_str());

  return i;
}

// Tom's function
int mjXBase::ReadSubElem(TiXmlElement* elem, std::string name, const int len,
                         double* data, std::string& text, bool required,
                         bool exact) {
  TiXmlElement* temp = FindSubElem(elem, name, required);
  if (!temp)
    if (!required)
      return 0;
    else
      throw mjXError(elem, "missing sub element: '%s'", name.c_str());
  else
    return ReadContent(temp, len, data, text, required, exact);
}

// Tom's function
bool mjXBase::ReadSubElemTxt(TiXmlElement* elem, const std::string name,
                             std::string& text, bool required) {
  TiXmlElement* temp = FindSubElem(elem, name, required);
  if (!temp) {
    if (required)
      throw mjXError(elem, "missing sub element (txt): '%s'", name.c_str());
    else
      return false;
  }
  text = temp->GetText();
  return true;
}

//------------------ untility functions: write ---------------------------------

// check if double is int
static bool isint(double x) {
  return ((fabs(x - floor(x)) < 1E-12) || (fabs(x - ceil(x)) < 1E-12));
}

// round to nearest int
static int Round(double x) {
  if (fabs(x - floor(x)) < fabs(x - ceil(x)))
    return (int)floor(x);
  else
    return (int)ceil(x);
}

// write attribute- double
void mjXBase::WriteAttr(TiXmlElement* elem, std::string name, int n,
                        double* data, const double* def) {
  char buf[100];
  std::string value;
  value.clear();

  // make sure all are defined
  for (int i = 0; i < n; i++)
    if (isnan(data[i])) return;

  // skip default attributes
  if (def) {
    bool skip = true;
    for (int i = 0; i < n; i++)
      if (fabs(data[i] - def[i]) > 1E-10) skip = false;

    if (skip) return;
  }

  // process all numbers
  for (int i = 0; i < n; i++) {
    // add space between numbers
    if (i > 0) value = value + " ";

    // write integer or float
    if (isint(data[i]))
      sprintf(buf, "%d", Round(data[i]));
    else
      sprintf(buf, "%g", data[i]);

    // append number
    value = value + buf;
  }

  // set attribute as string
  WriteAttrTxt(elem, name, value);
}

// write attribute- float
void mjXBase::WriteAttr(TiXmlElement* elem, std::string name, int n,
                        float* data, const float* def) {
  char buf[100];
  std::string value;
  value.clear();

  // skip default attributes
  if (def) {
    bool skip = true;
    for (int i = 0; i < n; i++)
      if (fabs(data[i] - def[i]) > 1E-7) skip = false;

    if (skip) return;
  }

  // process all numbers
  for (int i = 0; i < n; i++) {
    // add space between numbers
    if (i > 0) value = value + " ";

    // write integer or float
    if (isint(data[i]))
      sprintf(buf, "%d", Round(data[i]));
    else
      sprintf(buf, "%g", data[i]);

    // append number
    value = value + buf;
  }

  // set attribute as string
  WriteAttrTxt(elem, name, value);
}

// write attribute- string
void mjXBase::WriteAttrTxt(TiXmlElement* elem, std::string name,
                           std::string value) {
  // skip if value is empty
  if (value.empty()) return;

  // set attribute
  elem->SetAttribute(name.c_str(), value.c_str());
}

// write attribute- single int
void mjXBase::WriteAttrInt(TiXmlElement* elem, std::string name, int data,
                           int def) {
  // skip default
  if (data == def) return;

  elem->SetAttribute(name.c_str(), data);
}

// write attribute- keyword
void mjXBase::WriteAttrKey(TiXmlElement* elem, std::string name,
                           const mjMap* map, int mapsz, int data, int def) {
  // skip default
  if (data == def) return;

  WriteAttrTxt(elem, name, FindValue(map, mapsz, data));
}
