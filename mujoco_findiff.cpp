#include "mpc_studio.h"

void stepSkip(const mjModel* m, mjData* d) 
{
  mj_forwardSkip(m, d, 2, 0);

  if (m->opt.integrator == mjINT_RK4)
    mj_RungeKutta(m, d, 4);
  else
    mj_Euler(m, d);

  mj_fwdPosition(m, d);
  mj_fwdVelocity(m, d);
}


void multiStep(const mjModel* m, mjData* d, bool preUpdatePosVel, int nstep) 
{
  if(preUpdatePosVel)
  {
    mj_fwdPosition(m, d);
    mj_fwdVelocity(m, d);    
  }

  for (int i = 0; i < nstep; i++) 
    stepSkip(m, d);
  
  // Now they are called automatically
  /*mj_sensorPos(m, d);
  mj_sensorVel(m, d);
  mj_sensorAcc(m, d);
  mj_energyPos(m, d);  
  mj_energyVel(m, d);*/
}


void mjd_simple_dyn(const mjModel *m, const mjResidual *residual, mjData *d, mjtNum *w, const mjtNum *z) 
{

  int nq = m->nq;           
  int nv = m->nv;           
  int nu = m->nu;           
  int na = m->na;           
  int nx = nq + nv + na;    
  int nr = residual->max_nr;

  // ===== copy input from z =====
  mju_copy(d->qpos, z, nq);           // position
  mju_copy(d->qvel, z + nq, nv);      // velocity
  mju_copy(d->act, z + nq + nv, na);  // activation
  mju_copy(d->ctrl, z + nx, nu);      // control

  multiStep(m, d, true, residual->nTimesteps);

  // get features
  if (nr > 0) 
    get_residual(w + nx, 0, m, residual, d, -1);  

  // ===== copy output to w =====
  mju_copy(w, d->qpos, nq);           // position
  mju_copy(w + nq, d->qvel, nv);      // velocity
  mju_copy(w + nq + nv, d->act, na);  // activation

  // correct for the modification of time:
  d->time -= m->opt.timestep; 
}


void mjd_simple_diff(const mjModel *m, const mjResidual *residual, mjData *d, trajElement *e)
{
  mjMARKSTACK
  int nq = m->nq;           /* configuration*/
  int nv = m->nv;           /* velocity */
  int nu = m->nu;           /* controls */
  int na = m->na;           /* dynamic actuators*/
  int nx = nq + nv + na;    /* state */
  int nz = nx + nu;        /* input */
  int nr = residual->max_nr;       /* residual */
  int nw = nx + nr;     /* output */
  int i;

  // hardcoded constant, make options?
  mjtNum eps = mju_pow(2.0, -19.0);

  mjtNum *z, *wz, *wzT, *wu;
  mjtNum *dz, *z_p, *dw, *w, *w1, *w2;


  // mj_stack variables
  z = mj_stackAlloc(d, nz);
  wz = mj_stackAlloc(d, nw * nz);   // dw/dz
  wzT = mj_stackAlloc(d, nw * nz);  // dw/dz'
  mju_zero(wzT, nw * nz);

  // more mj_stack variables
  dz = mj_stackAlloc(d, nz);  // perturbation
  z_p = mj_stackAlloc(d, nz);  // perturbed input
  dw = mj_stackAlloc(d, nw);  // contracted output
  w = mj_stackAlloc(d, nw);    // output
  w1 = mj_stackAlloc(d, nw);   // perturbed output
  w2 = mj_stackAlloc(d, nw);   // perturbed output

  // get input z
  mju_copy(z, e->x, nx);
  mju_copy(z + nx, e->u, nu);

  // central cloud point (with analytic activation derivatives)
  mjd_simple_dyn(m, residual, d, w, z);

  // copy residual from central point
  mju_copy(e->r, w + nx, nr);

  // all other points
  for (i = nz-1; i >= 0; i--)
  {
    // perturbation
    mju_zero(dz, nz);
    dz[i] = eps;

    // add
    mju_add(z_p, z, dz, nz);


    mjd_simple_dyn(m, residual, d, w1, z_p);

    // diff
    mju_sub(dw, w1, w, nw);

    // scale
    mju_scl(wzT + nw*i, dw, 1 / eps, nw);

  }

  // transpose
  mju_transpose(wz, wzT, nz, nw);

  for (i = 0; i < nw * nz; i++)
    if (mju_isBad(wz[i])) 
      wz[i] = 0;

  // copy rz
  mju_copy(e->rz, wz + nx * nz, nr * nz);

  // transpose and copy yx
  mju_transpose(wz, wzT, nx, nw);
  mju_copy(e->yx, wz, nx * nx);

  // transpose and copy yu
  wu = wz + nw * nx;
  mju_transpose(wu, wzT + (nx * nw), nu, nw);
  mju_copy(e->yu, wu, nx * nu);

  mjFREESTACK
}


      // if (!central)  // forward finite-differencing
      // {
      //   // diff
      //   mjd_diff(m, s, d, dw, w1, w, do_inverse);

      //   // scale
      //   mju_scl(wzT + ndw * i, dw, 1 / eps, ndw);

      // } else  // central finite-differencing
      // {
      //   // perturbation
      //   mju_zero(dz, ndz);
      //   dz[i] = -eps;

      //   // add
      //   mjd_add(m, s, d, z_p, z, dz);

      //   // dynamics
      //   mjd_dyn(m, residual, d, w2, z_p, s->pert[i], dz, do_inverse, isFinal);

      //   // diff
      //   mjd_diff(m, s, d, dw, w1, w2, do_inverse);

      //   // scale
      //   mju_scl(wzT + ndw * i, dw, 1 / (2 * eps), ndw);
      // }