#include "util.h"

// allocate size mjtNums on scratch
mjtNum* tt_stackAlloc(mjtNum** scratch, int* szScratch, int size) {
  mjtNum* result;

  // check size
  if (*szScratch < size) mju_error("stack overflow");

  result = (mjtNum*)*scratch;
  *szScratch -= size;
  *scratch += size;
  return result;
}

void tt_copyDF(mjtNum* dest, const float* source, int n) {
  int i;
  for (i = 0; i < n; i++) dest[i] = source[i];
}

void tt_copyFD(float* dest, const mjtNum* source, int n) {
  int i;
  for (i = 0; i < n; i++) dest[i] = (float)source[i];
}

//------------------------------ actuator models -------------------------------

// smooth abs, output y = sqrt(x^2+beta^2)-beta, yx= dy/dx
mjtNum tt_softAbs(mjtNum x, mjtNum beta, mjtNum* yx) {
  mjtNum tmp;
  if (beta == 0)  // hard abs
  {
    if (yx) *yx = (x >= 0 ? 1 : -1);
    return (x >= 0 ? x : -x);
  } else  // smooth abs
  {
    tmp = sqrt(x * x + beta * beta);
    if (yx) *yx = x / tmp;
    return tmp - beta;
  }
}

// long-tailed sigmoid, y = x/sqrt(x^2+beta^2)
mjtNum tt_sigmoid(mjtNum x, mjtNum beta, mjtNum* yx, mjtNum* yxx) {
  mjtNum s, y;
  if (beta == 0)  // step function
  {
    if (yx) *yx = 0;
    if (yxx) *yxx = 0;
    return (x > 0 ? 1 : -1);
  } 
  else  // sigmoid
  {
    s = 1 / sqrt(x * x + beta * beta);
    y = s * x;
    if (yx) *yx = s * (1 - y * y);
    if (yxx) *yxx = -3 * s * y * yx[0];
    return y;
  }
}

mjtByte tt_isBadVec(const mjtNum* x, int n) {
  int i;
  for (i = 0; i < n; i++)
    if (mju_isBad(x[i])) return 1;
  return 0;
}

// Write to a file
void tt_fWrite(FILE* fp, mjtNum* data, int n, int newline_at_end) {
  int i;
  for (i = 0; i < n; i++) fprintf(fp, "%f,\t", data[i]);

  if (newline_at_end) fprintf(fp, "\n");
}

// enforce symmetry
void tt_symmetrize(mjtNum* mat, int n) {
  int i, j;
  mjtNum tmp;

  for (i = 0; i < n; i++)
    for (j = 0; j < i; j++) {
      tmp = 0.5 * (mat[i * n + j] + mat[j * n + i]);
      mat[i * n + j] = mat[j * n + i] = tmp;
    }
}


// smooth max function
mjtNum tt_softMax(mjtNum x, mjtNum y, mjtNum beta) {
  return (x + y + tt_softAbs(x - y, beta, 0)) / 2;
}

// smooth min function
mjtNum tt_softMin(mjtNum x, mjtNum y, mjtNum beta) {
  return (x + y - tt_softAbs(x - y, beta, 0)) / 2;
}

mjtNum tt_rand_gauss(void) {
  mjtNum v1, v2, s;

  do {
    v1 = 2.0 * ((mjtNum)rand() / RAND_MAX) - 1;
    v2 = 2.0 * ((mjtNum)rand() / RAND_MAX) - 1;
    s = v1 * v1 + v2 * v2;
  } while (s >= 1.0);

  if (s == 0.0)
    return 0.0;
  else
    return (v1 * mju_sqrt(-2.0 * mju_log(s) / s));
}

void tt_thread_schedule(int schedule[][2], int nOperation, int nThread) {
  int i, blksz, extra;
  // prepare thread schedule
  blksz = nOperation / nThread;
  extra = nOperation - blksz * nThread;
  for (i = 0; i < nThread; i++) {
    schedule[i][0] = (i ? schedule[i - 1][1] : 0);
    schedule[i][1] = schedule[i][0] + blksz + (i < extra);
  }
}


// identity matrix
void tt_eye(mjtNum* mat, int n) {
  int i;
  mju_zero(mat, n * n);
  for (i = 0; i < n; i++) mat[i * n + i] = 1.0;
}

// fill buffer with value
void tt_fill(mjtNum* vec, mjtNum val, int n) {
  int i;
  for (i = 0; i < n; i++) vec[i] = val;
}


// sub-tree quantities (com, com velocity, angular momentum)
void mj_subtree(const mjModel* m, mjData* d, mjtNum* com, mjtNum* com_vel,
                mjtNum* momentum) {
  int i, parent;
  mjtNum *mass, *lin_mom, *ang_mom, *body_vel, dv[3], dL[3], dx[3], dp[3];

  mjMARKSTACK mass = mj_stackAlloc(d, m->nbody);
  lin_mom = mj_stackAlloc(d, 3 * m->nbody);
  ang_mom = mj_stackAlloc(d, 3 * m->nbody);
  body_vel = mj_stackAlloc(d, 6 * m->nbody);

  // bodywise quantities
  for (i = 0; i < m->nbody; i++) {
    // position x mass
    mju_scl3(com + 3 * i, d->xipos + 3 * i, m->body_mass[i]);

    // body velocity
    mj_objectVelocity(m, d, mjOBJ_BODY, i, body_vel + 6 * i, 0);   

    // body linear momentum
    mju_scl3(lin_mom + 3 * i, body_vel + 6 * i + 3, m->body_mass[i]);

    // body angular momentum
    mju_rotVecMatT(dv, body_vel + 6 * i, d->ximat + 9 * i);
    dv[0] *= m->body_inertia[3 * i];
    dv[1] *= m->body_inertia[3 * i + 1];
    dv[2] *= m->body_inertia[3 * i + 2];
    mju_rotVecMat(ang_mom + 3 * i, dv, d->ximat + 9 * i);
  }

  // init subtree mass
  mju_copy(mass, m->body_mass, m->nbody);

  // subtree com and com_vel
  for (i = m->nbody - 1; i >= 0; i--) {
    if (i) {
      parent = m->body_parentid[i];
      // add scaled positions and velocities
      mju_addTo3(com + 3 * parent, com + 3 * i);
      mju_addTo3(lin_mom + 3 * parent, lin_mom + 3 * i);
      // accumulate mass
      mass[parent] += mass[i];
    }
    // divide by subtree mass
    mju_scl3(com + 3 * i, com + 3 * i, 1 / mass[i]);
    mju_scl3(com_vel + 3 * i, lin_mom + 3 * i, 1 / mass[i]);
  }

  // subtree angular momentum
  for (i = m->nbody - 1; i > 0; i--) {
    parent = m->body_parentid[i];

    // momentum wrt body i
    mju_sub3(dx, d->xipos + 3 * i, com + 3 * i);
    mju_sub3(dv, body_vel + 6 * i + 3, com_vel + 3 * i);
    mju_scl3(dp, dv, m->body_mass[i]);
    mju_cross(dL, dx, dp);

    // add to subtree i
    mju_addTo3(ang_mom + 3 * i, dL);

    // add to parent
    mju_addTo3(ang_mom + 3 * parent, ang_mom + 3 * i);

    // momentum wrt parent
    mju_sub3(dx, com + 3 * i, com + 3 * parent);
    mju_sub3(dv, com_vel + 3 * i, com_vel + 3 * parent);
    mju_scl3(dv, dv, mass[i]);
    mju_cross(dL, dx, dv);

    // add to parent
    mju_addTo3(ang_mom + 3 * parent, dL);
  }

  // momentum = [ang_mom lin_mom]
  for (i = 0; i < m->nbody; i++) {
    mju_copy(momentum + 6 * i, ang_mom + 3 * i, 3);
    mju_copy(momentum + 6 * i + 3, lin_mom + 3 * i, 3);
  }

  mjFREESTACK
}