#include "mpc_studio.h"

// table of all norms. format:
// {name, type, isloss, nparam, number of expected inputs (0: any)}
mjtNorm funList[NUM_NORM] = {
  {"identity",         mjLOSS_IDENTITY,    1, 0, 0},
  {"quadratic",        mjLOSS_QUADRATIC,   1, 0, 0},
  {"power",            mjLOSS_POWER,       1, 1, 0},
  {"exponential",      mjLOSS_EXP,         1, 1, 0},
  {"smooth_abs",       mjLOSS_SMOOTH_ABS,  1, 1, 0},
  {"smooth_abs2",      mjLOSS_SMOOTH_ABS2, 1, 2, 0},
  {"huber",            mjLOSS_HUBER,       1, 1, 0},  
  {"huber2",           mjLOSS_HUBER2,      1, 2, 0},    
  {"rectify",          mjLOSS_RECTIFY,     1, 1, 0},
  {"rectify2",         mjLOSS_RECTIFY2,    1, 2, 0},
  {"quadratic bias" ,  mjLOSS_QUAD_BIAS,   1, 1, 0},
  {"sigmoid",          mjLOSS_SIGMOID,     1, 1, 0},
  {"step",             mjLOSS_STEP,        1, 1, 0},
  {"clip",             mjLOSS_CLIP,        1, 1, 0},
  {"sine",             mjLOSS_SINE,        1, 0, 0},
  {"cosine",           mjLOSS_COSINE,      1, 0, 0},
  {"log",              mjLOSS_LOG,         1, 0, 0},
  {"identity",         mjNORM_SUM,         0, 0, 0},
  {"product",          mjNORM_PROD,        0, 0, 0},
  {"bilinear",         mjNORM_BILINEAR,    0, 0, 0},
  {"L2",               mjNORM_L2,          0, 1, 0},
  {"L22",              mjNORM_L22,         0, 2, 0},
  {"gaussian",         mjNORM_GAUSSIAN,    0, 1, 0}};

mjtNum mj_norm(mjtNum *g, mjtNum *H, const mjtNum *z, int n, const mjtNum *params,
              mjtNormType type) {
  mjtNum x = z[0];                                                // input
  mjtNum y = 0;                                                   // output
  mjtNum p = params ? params[0] : 0, q = params ? params[1] : 0;  // parameters

  int i, j, m;
  mjtNum a, b, c, d, e, s, sx;

  if ((type < mjNORM_SUM) && (n != 1))
    mju_error("loss expects a single input feature");

  switch (type) {
    case mjLOSS_IDENTITY:  // identity, y = x
      y = x;
      if (g) *g = 1;
      if (H) *H = 0;
      break;

    case mjLOSS_QUADRATIC:  // quadratic, y = 0.5*x^2
      y = x * x / 2;
      if (g) *g = x;
      if (H) *H = 1;
      break;

    case mjLOSS_POWER:  // power, y = abs(x)^p
      s = mju_abs(x);
      y = mju_pow(s, p);
      if (g) *g = mju_sign(x) * p * mju_pow(s, p - 1);
      if (H) *H = (p - 1) * p * mju_pow(s, p - 2);
      break;

    case mjLOSS_EXP:  // exponential, y = exp(p*x))
      y = mju_exp(x * p);
      if (g) *g = p * y;
      if (H) *H = p * p * y;
      break;

    case mjLOSS_SMOOTH_ABS:  // smooth_abs, y = sqrt(x^2 + p^2) - p
      s = mju_sqrt(x * x + p * p);
      y = s - p;
      if (g) *g = s ? x / s : 0;
      if (H) *H = s ? (1 - g[0] * g[0]) / s : 0;
      break;

    case mjLOSS_SMOOTH_ABS2:  // smooth_abs2, y = (abs(x)^q + p^q)^(1/q) - p
      q = params[1];
      a = mju_abs(x);
      d = mju_pow(a, q);
      e = d + mju_pow(p, q);
      s = mju_pow(e, 1 / q);
      y = s - p;
      if (g || H) c = s * mju_pow(a, q - 2) / e;
      if (g) *g = c * x;
      if (H) *H = c * (q - 1) * (1 - d / e);
      break;

    case mjLOSS_HUBER:  // y = abs(x)<p ? x^2/p/2 : abs(x)-p/2
      s = mju_abs(x);
      i = (int)(s<p);
      y = i ? x*x/p/2 : s-p/2;
      if (g) *g = i ? x/p : mju_sign(x);
      if (H) *H = i ? 1/p : 0;
      break;      

    case mjLOSS_HUBER2:  // y = abs(x)<p ? abs(x/p)^q*p/q : abs(x)-p*(q-1)/q
      s = mju_abs(x);
      i = (int)(s<p);
      y = i ? mju_pow(s/p,q)*p/q : s-p*(q-1)/q;
      if (g) *g = i ? y*q/x : mju_sign(x);
      if (H) *H = i ? (*g)*(q-1)/x : 0;
      break;      

    case mjLOSS_RECTIFY:  // y  =  p*log(1 + exp(x/p))
      if (p > 0) {
        s = mju_exp(x / p);
        y = p * mju_log(1 + s);
        if (g) *g = s / (1 + s);
        if (H) *H = s / (p * (1 + s) * (1 + s));
      } else {
        y = x > 0 ? x : 0;
        if (g) *g = x > 0 ? 1 : 0;
        if (H) *H = 0;
      }
      break;

    case mjLOSS_RECTIFY2:  // rectify2, y  =  (q*x + sqrt(x^2 + p^2)) / 2,
      q = params[1];
      s = mju_sqrt(x * x + p * p);
      y = (s + q * x) / 2;
      sx = x / s;
      if (g) *g = (sx + q) / 2;
      if (H) *H = (1 - sx * sx) / (2 * s);
      break;

    case mjLOSS_QUAD_BIAS:  // quadratic with bias, y = 0.5*(x-p)^2
      s = x - params[0];
      y = 0.5 * s * s;
      if (g) *g = s;
      if (H) *H = 1;
      break;

    case mjLOSS_SIGMOID:  // long-tailed sigmoid, y = x/sqrt(x^2+beta^2)
      y = tt_sigmoid(x, params[0], g ? g : 0, H ? H : 0);
      break;

    case mjLOSS_STEP:  // step function, y = (1 + sigmoid(x))/2
      y = (1 + tt_sigmoid(x, params[0], g ? g : 0, H ? H : 0)) / 2;
      if (g) *g /= 2;
      if (H) *H /= 2;
      break;

    case mjLOSS_CLIP: // clip
      y = x>p ? p : x;
      if( g ) *g = x>p ? 0 : 1;
      if( H ) *H = 0;
      break;

    case mjLOSS_SINE:  // sine, y = sin(x)
      y = mju_sin(x);
      if (g) *g = cos(x);
      if (H) *H = -y;
      break;

    case mjLOSS_COSINE:  // cosine, y = cos(x)
      y = mju_cos(x);
      if (g) *g = -sin(x);
      if (H) *H = -y;
      break;

    case mjLOSS_LOG:  // log, y = log(x)
      y = mju_log(x);
      if (g) *g = 1 / x;
      if (H) *H = -1 / x / x;
      break;

    case mjNORM_SUM:  // sum, y = sum(x)
      for (i = 0; i < n; i++) y += z[i];
      if (g) tt_fill(g, 1.0, n);
      if (H) mju_zero(H, n * n);
      break;

    case mjNORM_PROD:  // product, y = prod(x)
      y = 1;
      for (i = 0; i < n; i++) y *= z[i];
      if (g)
        for (i = 0; i < n; i++) g[i] = y / z[i];
      if (H)
        for (i = 0; i < n; i++)
          for (j = 0; j < n; j++) H[i + j * n] = i == j ? 0 : y / (z[i] * z[j]);
      break;

    case mjNORM_BILINEAR:  // bilinear, y = sum(x(0:n/2-1).*x(n/2:end))
      m = n / 2;
      if (m != 2 * n)
        mju_error("bilinear norm expects an even number of inputs");
      for (i = 0; i < m; i++) {
        j = m + i;
        y += z[i] * z[j];
        if (g) {
          g[i] = z[j];
          g[j] = z[i];
        }
        if (H) mju_zero(H + n * i, n);
      }
      break;

    case mjNORM_L2:  // smooth L2, y = sqrt(x*x' + p^2) - p
      s = mju_dot(z, z, n);
      s = mju_sqrt(s + p * p);
      y = s - p;
      if (g) {
        if (s) {
          mju_scl(g, z, 1/s, n);
        } else {
          mju_zero(g, n);
        }
      }

      if (H) { // H = (eye(n) - g*g')/s
        if (s) {
          for (i = 0; i < n; i++)
            for (j = 0; j < n; j++)
              H[i + j * n] = ((i == j ? 1 : 0) - g[i] * g[j]) / s;
        } else {
          mju_zero(H, n*n);
        }
      }
      break;

    case mjNORM_L22:  // L22, y = ((x*x')^q + p^(2*q))^(1/2/q) - p
      c = 0;
      for (i = 0; i < n; i++) c += z[i] * z[i];
      a = mju_pow(c, q / 2) + mju_pow(p, q);
      s = mju_pow(a, 1 / q);
      y = s - p;
      d = mju_pow(c, q / 2 - 1);
      b = s / a * d;
      if (g)
        for (i = 0; i < n; i++) g[i] = b * z[i];

      if (H) {
        c = (1 - q) * d / a + (q - 2) / c;
        for (i = 0; i < n; i++)
          for (j = 0; j < n; j++)
            H[i + j * n] = b * ((i == j ? 1.0 : 0.0) + z[i] * z[j] * c);
      }
      break;

    case mjNORM_GAUSSIAN:  // gaussian, y = exp(sum(-((x/p)^2/2))
      p = 1/(p*p);
      y = mju_exp(-p * mju_dot(z, z, n) / 2);
      if (g)
        for (i = 0; i < n; i++) g[i] = -p * y * z[i];
      if (H)
        for (i = 0; i < n; i++)
          for (j = 0; j < n; j++)
            H[i + j * n] = p * y * (p * z[i] * z[j] + (i == j ? -1 : 0));
      break;

    default:
      mju_error("mj_norm: unknown norm type");
  }
  return y;
}
