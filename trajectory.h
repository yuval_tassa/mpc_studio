#ifndef _TT_TRAJECTORY_H_
#define _TT_TRAJECTORY_H_


#include "util.h"

#define N_TRJ 28
#define N_TRJ_TIMER 5
#define N_CB_TIMER 12

typedef enum _trjEnableBit {
  trTIME = 1 << 0,
  trTIMESTEP = 1 << 1,
  trSTATE = 1 << 2,
  trCONTROL = 1 << 3,
  trUSER = 1 << 4,
  trMINIMAL = trTIME + trTIMESTEP + trSTATE + trCONTROL + trUSER,

  trRESIDUAL = 1 << 5,
  trCOST = 1 << 6,
  trFEATURES = 1 << 7,
  trBASIC = trMINIMAL + trRESIDUAL + trCOST + trFEATURES,

  trYX = 1 << 8,
  trYU = 1 << 9,
  trYDERIVS = trYX + trYU,

  trRZ = 1 << 10,
  trDERIVS = trRZ + trYDERIVS,

  trCX = 1 << 11,
  trCU = 1 << 12,
  trCXX = 1 << 13,
  trCXU = 1 << 14,
  trCUU = 1 << 15,
  trCDERIVS = trCX + trCU + trCXX + trCXU + trCUU,

  trCR = 1 << 16,
  trCRR = 1 << 17,  
  trCRDERIVS = trCR + trCRR,

  trK = 1 << 18,
  trXK = 1 << 19,
  trFEEDBACK = trK + trXK,

  trDU = 1 << 20,
  trVX = 1 << 21,
  trVXX = 1 << 22,
  trRICATTI = trFEEDBACK + trDU + trVX + trVXX,

  trQX = 1 << 23,
  trQU = 1 << 24,
  trQXX = 1 << 25,
  trQXU = 1 << 26,
  trQUU = 1 << 27,
  trHAMILTONIAN = trQX + trQU + trQXX + trQXU + trQUU,

  trLQG = trBASIC + trDERIVS + trCDERIVS + trRICATTI,
  trROLLOUT = trBASIC + trFEEDBACK,
  trPONTRYAGIN =
      trBASIC + trDERIVS + trCDERIVS + trFEEDBACK + trVX + trVXX + trHAMILTONIAN
} trjEnableBit;

/*------------------ trajElement: a single trajectory time-step
 * ----------------------*/
// struct _trj;
// typedef struct _trj trajElement;
typedef struct _trajElement {
  // trBASIC: basic quantitites
  mjtNum *t;     // time 				(1)
  mjtNum *dt;    // timestep				(1)
  mjtNum *x;     // state				(nx)
  mjtNum *u;     // control				(nu)
  mjtNum *user;  // userdsata			(nuser)
  mjtNum *r;     // residual				(nr)
  mjtNum *c;     // cost	(vector)		(nc)
  mjtNum *f;     // features				(nf)

  // trDERIVS: state and residual derivatives
  mjtNum *yx;  //						(ndx x ndx)
  mjtNum *yu;  //						(ndx x nu)
  mjtNum *rz;  //						(nr  x (ndx+nu))

  // trCDERIVS: cost derivatives
  mjtNum *cx;   //						(ndx)
  mjtNum *cu;   //						(nu)
  mjtNum *cxx;  //						(ndx x ndx)
  mjtNum *cxu;  //						(ndx x nu)
  mjtNum *cuu;  //						(nu  x nu)
  mjtNum *cr;   //            (nr)
  mjtNum *crr;  //            (nr  x nr)  

  // trFEEDBACK: locally linear policy
  mjtNum *K;   // control gains		(nu  x ndx)
  mjtNum *xk;  // feedback set-point	(nx)

  // trRICATTI: computed by Riccati
  mjtNum *du;   // control modification	(nu)
  mjtNum *Vx;   // cost-to-go gradient	(ndx)
  mjtNum *Vxx;  // cost-to-go Hessian	(ndx x ndx)

  // trHAMILTONIAN: Hamiltonian
  mjtNum *Qx;   //						(ndx)
  mjtNum *Qu;   //						(nu)
  mjtNum *Qxx;  //						(ndx x ndx)
  mjtNum *Qxu;  //						(ndx x nu)
  mjtNum *Quu;  //						(nu x nu)
} trajElement;

extern const char *trjElement_names[N_TRJ];

// negative values are bad
typedef enum _mjoResult {
  mjOPT_FAIL = -5,              // unknown failure mode
  mjOPT_DIVERGENCE = -4,        // numerical divergence
  mjOPT_NEGDEF = -3,            // negative-definite Hessian
  mjOPT_NO_IMPROVEMENT = -2,    // no improvement found along search direction
  mjOPT_MUMAX = -1,             // regularizer reached maximum value
  mjOPT_SUCCESS = 0,            // succesful step
  mjOPT_TOLFUN = 1,             // change of objective smaller than tolerance
  mjOPT_TOLX = 2,               // change of x smaller than tolerance
  mjOPT_TOLG = 3,               // change of gradient smaller than tolerance
  mjOPT_FULLY_CONSTRAINED = 4,  // all inequality constraints are active

} mjoResult;

typedef struct _mjoTrace {
  // optimization
  mjoResult outcome;              // outcome of last optimization step
  mjtNum cost;                     // total cost
  mjtNum improvement;              // cost improvement  (old - new)
  mjtNum timing[N_TRJ_TIMER];      // optimization timing
  mjtNum timingCB[2][N_CB_TIMER];  // internal timing from callbacks
                                  // Levenberg-Marquardt
  mjtNum mu, dmu;  // Levenberg-Marquardt regularizer and its scaling coefficient
  mjtNum alpha;    // line-search coefficient
  mjtNum z;        // actual/expected reduction ratio
                  // iLQG
  mjtNum gradnorm;    // gradient norm
  mjtNum dV[2];       // Armijo coeffcients
                     // MPC
  mjtNum policy_lag;  // policy lag
  int winner;        // rollout winner
} mjoTrace;

/*------------- trajectory: an array of trajElement's and their data
 * -----------------*/
typedef struct _trajectory {
  // struct-array of N trjs
  trajElement *e;

  // allocation flags
  int flg;

  // sizes
  int N;  // time-steps in the trajectory
  int nx, nu, nf, nc, nr,
      ndx;             // size of state, control, cost, residual, diff-space
  int nuser;           // size of userdata
  int nstack, pstack;  // stack size, stack pointer

  // box limits on controls (u)
  mjtNum *lower, *upper;  // control limits

  // rollout modifiers
  mjtNum feedback;    // feedback scaling coefficient
  mjtNum time_shift;  // temporal offset

  mjoTrace trace;  // results from optimization step

  // data buffer
  int szBuffer;   // size of buffer
  mjtNum *buffer;  // start of buffer

} trajectory;

// sizes of different trajectory elements
void trjSizes(int sz[N_TRJ][2], int nx, int nu, int nf, int nr, int nc, int ndx,
              int nuser);

// allocate and initialize trajectory structure
trajectory *newTrajectory(int nx, int nu, int nf, int nr, int nc, int ndx,
                          int nuser, int N, int flg);

// set trajectory pointers
void setPtrTrajectory(trajectory *T);

// copy one trajectory into another
void copyTrajectory(trajectory *Tout, const trajectory *Tin);

// create a new trajectory and copy into it
trajectory *cloneTrajectory(const trajectory *Tin, int flg);

// interpolate trajectory at time t
void interpTrajectory(trajElement *e, const trajectory *T, mjtNum t, int align,
                      int extrap, int ignoreLast);

// clear a trajectory
void zeroTrajectory(trajectory *T);

// de-allocate trajectory
void deleteTrajectory(trajectory *T);

// serialize a trajectory
// int saveTrajectory(const trajectory* T, int szbuf, char* buffer);

// de-serialize a trajectory
// trajectory* loadTrajectory(trajectory* dest, int szbuf, void* buffer);

#endif
