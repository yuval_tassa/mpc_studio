#ifndef _TT_MUJOCO_FINDIFF_H_
#define _TT_MUJOCO_FINDIFF_H_


#include "mujoco.h"
#include "features.h"
#include "util.h"


// findiff utility functions
void mjd_simple_diff(const mjModel *m, const mjResidual *residual, mjData *d, trajElement *e);

void multiStep(const mjModel* m, mjData* d, bool preUpdatePosVel, int nstep);



#endif  // _TT_MUJOCO_FINDIFF_H_
