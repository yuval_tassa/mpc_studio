#ifndef _TT_UTIL_H_
#define _TT_UTIL_H_

#include "mujoco.h"
#include "stdio.h"
#include "math.h"
#include <stdarg.h>
#include "time.h"


// Windows
#ifdef _WIN32
  #define isnan _isnan
  #define strcasecmp _stricmp
#endif

//------------------------------ user handlers
//--------------------------------------

extern void (*tt_user_error)(const char*);
extern void (*tt_user_warning)(const char*);
extern void* (*tt_user_malloc)(unsigned int, unsigned int);
extern void (*tt_user_free)(void*);

mjtNum* tt_stackAlloc(mjtNum** scratch, int* szScratch, int size);

#ifdef __cplusplus
extern "C" {
#endif


void tt_copyDF(mjtNum* dest, const float* source, int n);

void tt_copyFD(float* dest, const mjtNum* source, int n);

// subtree quantities
void mj_subtree(const mjModel* m, mjData* d, mjtNum* com, mjtNum* com_vel,
                mjtNum* momentum);

// identity matrix
void tt_eye(mjtNum* mat, int n);

// fill vector with a value
void tt_fill(mjtNum* vec, mjtNum val, int n);

// rational sigmoid
mjtNum tt_sigmoid(mjtNum x, mjtNum beta, mjtNum* yx, mjtNum* yxx);

// a vectorized isbad function
mjtByte tt_isBadVec(const mjtNum* x, int n);

// enforce symmetry
void tt_symmetrize(mjtNum* mat, int n);

// smooth max function
mjtNum tt_softMax(mjtNum x, mjtNum y, mjtNum beta);

// smooth min function
mjtNum tt_softMin(mjtNum x, mjtNum y, mjtNum beta);

// smooth abs, output y = sqrt(x^2+beta^2)-beta, yx= dy/dx
mjtNum tt_softAbs(mjtNum x, mjtNum beta, mjtNum* yx);

// gaussian random number generator
mjtNum tt_rand_gauss(void);

// prepare thread schedule
void tt_thread_schedule(int schedule[][2], int nOperation, int nThread);

#ifdef __cplusplus
}
#endif


#endif  // _TT_UTIL_H_
