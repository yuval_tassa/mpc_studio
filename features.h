#ifndef _TT_FEATURES_H_
#define _TT_FEATURES_H_

#include "util.h"
#include "cost.h"

#define MAX_N_COSTS 5

#define NUM_BUFFER_FIELDS 56
#define NUM_FIELDS 77
// feature Enum
#define MAX_NF 2700
#define MAX_NR 500
#define MAX_NCF 4
#define MAX_OBS MAX_NR

// constants for diff-space
#define MAX_NDX 200
#define MAX_NDZ 200
#define MAX_NSUBSPACE 20
#define TAG_IGNORE 9000

typedef enum _mjtFEATURE_FIELD {
  mjFIELD_DT  = 0,
  mjFIELD_TIME,
  mjFIELD_COM,
  mjFIELD_ENERGY,
  mjFIELD_XVEL,
  mjFIELD_GEOM_XVEL,
  mjFIELD_SITE_XVEL,
  mjFIELD_XVEL_LOCAL,
  mjFIELD_GEOM_XVEL_LOCAL,
  mjFIELD_SITE_XVEL_LOCAL,
  mjFIELD_XACC,
  mjFIELD_GEOM_XACC,
  mjFIELD_SITE_XACC,
  mjFIELD_XACC_LOCAL,
  mjFIELD_GEOM_XACC_LOCAL,
  mjFIELD_SITE_XACC_LOCAL,
  mjFIELD_SUBCOM,
  mjFIELD_SUBCOMVEL,
  mjFIELD_MOMENTUM,
  mjFIELD_SQRTMINV,
  mjFIELD_FILE,
  mjFIELD_FEATURE,
  mjFIELD_DATA,
  mjFIELD_CONST
} mjtFEATURE_FIELD;

typedef enum _extra_features {
  extftSUBTREE  = 1 << 0,
  extftRNEPOST  = 1 << 1,
  extftSQRTMINV = 1 << 2
} extra_features;

typedef struct _mjDatum {
  int field;
  int item;
  int entry;
  int buffer_adr;
  mjtNum coef;
  mjtNum ref;
} mjDatum;

typedef enum _mjtOperatorType {
  mj_OP_GET_DATUM = -1,
  mj_OP_CAT,        // f = [x;y]
  mj_OP_SUM,        // f = sum([x;y])
  mj_OP_MAX,        // f = max(x,y)
  mj_OP_LOSS,       // f = loss([x;y])
  mj_OP_NORM,       // f = norm([x;y])
  mj_OP_ADD,        // f = x+y
  mj_OP_PROD,       // f = x.*y
  mj_OP_DOT,        // f = x'*y
  mj_OP_NORMALIZE,  // f = x/norm(x)   or   y.*x/norm(x)
  mj_OP_CROSS,      // f = cross(x,y)
  mj_OP_SINCOS,     // f = [sin(x), cos(x)]
  mj_OP_SEGPROJ,    // f = project y(0:2) onto the segment [x(0:2) x(3:5)]
  mj_OP_QUATDIFF,   // f = neg(y)*x (both x and y have 4 entries, f has 3)
  mj_OP_MULMATVEC,  // f = mat(y)*x   dim(y) = dim(x)*dim(x)
  mj_OP_MULMATTVEC  // f = mat(y)'*x  dim(y) = dim(x)*dim(x)
} mjtOperatorType;

#define NUM_OPERATORS 15

typedef struct _mjFeature {
  int fun;

  // these are non-empty iff fun == mj_OP_GET_DATUM
  int datums_adr;  // address in datums array
  int datums_size;

  // first input to fun, empty iff fun == mj_OP_GET_DATUM
  int in1_adr;  // address in features array
  int in1_size;

  // second input to fun, may be empty
  int in2_adr;  // address in features array
  int in2_size;

  mjtNum ref;   // default value is 0 (written by interpret_features)
  mjtNum coef;  // default value is 1 (written by interpret_features)
  mjtNum params[MAX_NORM_PARAM + 1];  // may be empty
} mjFeature;

typedef struct _mjResidual {
  int nTimesteps;

  int extra_flags;

  int max_nr;
  int max_nterms;
  int ncosts;
  int active_cost;
  int n_auxf;
  mjCost cost[MAX_N_COSTS];

  mjDatum datums[MAX_NF];
  mjFeature features[MAX_NR];
  int num_features;
  int nf;  // how big should f be - better overestimate this!
  int observationIndices[MAX_OBS];
  int observationLengths[MAX_OBS];
  int numObservations;
  char observationNames[MAX_OBS][100];

  int numCosts;
  char costNames[MAX_N_COSTS][100];

  mjtNum* pfiledata;
  int filesize;
  int rowlength;
  int currowstart;
} mjResidual;

void get_residual(mjtNum* r, mjtNum* f, const mjModel*, const mjResidual*,
                  mjData*, int id);

void get_residual_from_id(mjtNum* output, mjtNum* observations, const mjModel* m,
                          const mjResidual* residual, mjData* d, int id);

int mjf_operator(mjtNum* f, const mjtNum* x, int nx, const mjtNum* y, int ny,
                 mjtOperatorType type, const mjtNum* params);

typedef struct _mjFeatureOperation {
  const char* name;
  int nparam;
  int indim1;
  int indim2;
  mjtByte associative;
  mjtOperatorType opType;
} mjtFeatureOperation;

// feature i/o functions
int mj_sizeFC();
int mj_saveFC(mjResidual* residual, const char* filename, int szbuf, void* buf);
int mj_loadFC(mjResidual* residual, int max_nCosts, const char* filename,
              int szbuf, void* buf);


#endif
