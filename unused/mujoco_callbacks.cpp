#include "mujoco_callbacks.h"

// assumes:
//   nThread mjData's are prepared in D
//   nRollout candidates are allocated and prepared
//   policy can be NULL
//   yuval: what if cbData.residual==NULL ?
const int MAX_THREADS = 12;

int multiRollout(void *cbData, const trajectory *policy,
                 trajectory **candidates) {
  mjoData data = *(mjoData *)cbData;
  const mjModel *m = data.m;
  const mjResidual *residual = data.r;
  mjData **D = data.D;

  int i, j, winner;
  mjtNum lowest_cost = -mju_log(0.0);
  mjOptOpt opts = residual->cost[residual->active_cost].optopt;
  int nThreads = mjMIN(MAX_THREADS, opts.nThreads);
  int schedule[MAX_THREADS][2];

  // prepare thread schedule
  tt_thread_schedule(schedule, opts.nRollout, nThreads);

// do rollouts
#pragma omp parallel for private(j) schedule(static) num_threads(nThreads)
  for (i = 0; i < nThreads; i++)
    for (j = schedule[i][0]; j < schedule[i][1]; j++)
      rollout(m, residual, D[i], candidates[j], policy);

  // choose winner
  winner = -1;
  for (i = 0; i < opts.nRollout; i++)
    if (candidates[i]->trace.cost < lowest_cost) {
      lowest_cost = candidates[i]->trace.cost;
      winner = i;
    }

  // sum all candidate timers into winner
  if (winner >= 0)
    for (i = 0; i < opts.nRollout; i++)
      if (i != winner)
        mju_addTo(candidates[winner]->trace.timingCB[0],
                 candidates[i]->trace.timingCB[0], N_CB_TIMER);

  return winner;
}

int trajDerivatives(void *cbData, trajectory *policy) {
  mjoData data = *(mjoData *)cbData;
  mjModel *m = data.m;
  const mjResidual *residual = data.r;
  mjData **D = data.D;

  mjOptOpt opts = residual->cost[residual->active_cost].optopt;
  int schedule[MAX_THREADS][2];
  int nThreads = mjMIN(MAX_THREADS, opts.nThreads);
  int i, j, N = policy->N;
  mjtNum c = 0;
  const mjCost *cost = &residual->cost[residual->active_cost];

  // set diable flags
  m->opt.disableflags |= mjDSBL_WARMSTART;
  m->opt.disableflags |= mjDSBL_CLAMPCTRL;

  // prepare thread schedule
  tt_thread_schedule(schedule, N, nThreads);

  // clear mjData timers
  for (i = 0; i < nThreads; i++) mju_zero(D[i]->timer_duration, mjNTIMER);

//-------------------------- dynamic derivatives
//------------------------------------
#pragma omp parallel for private(j) schedule(static) num_threads(nThreads)
  for (i = 0; i < nThreads; i++)
    for (j = schedule[i][0]; j < schedule[i][1]; j++) {
      // populate with measurements
      mju_copy(D[i]->userdata, policy->e[j].user,
              mjMIN(m->nuserdata, policy->nuser));
      // get derivatives
      mjd_derivatives(m, j == N - 1, residual, D[i], policy->e + j, 0);
    }

  // add mjData timers into trajectory timers, convert to milliseconds
  for (i = 0; i < nThreads; i++)
    mju_addToScl(policy->trace.timingCB[1], D[i]->timer_duration, 1e-3,
                mjNTIMER);

//-------------------------- cost derivatives
//------------------------------------

#pragma omp parallel for private(j) schedule(static) num_threads(nThreads)
  for (i = 0; i < nThreads; i++)
    for (j = schedule[i][0]; j < schedule[i][1]; j++)
      cost_step_derivs(policy->e + j, cost, policy->nx, policy->ndx, policy->nu,
                j + 1 == N ? 1 : 0, policy->e[0].t[0],
                D[i]->stack + D[i]->pstack, D[i]->nstack - D[i]->pstack);

  // sanity check
  for (i = 0; i < N; i++)
    for (j = 0; j < policy->nc; j++) c += policy->e[i].c[j];

  printf("trace.cost: %f, c: %f \n", policy->trace.cost, c);

  if (mju_abs(policy->trace.cost - c) > mjMINVAL) {
    mju_warning(
        "cost computed by derivatives is different from cost computed by "
        "rollout");
    return 1;  // error
  }

  return 0;
}
