#ifndef _TT_ILQG_H_
#define _TT_ILQG_H_


// extensions
#include "trajectory.h"
#include "cost.h"
#include "boxQP.h"
#include "util.h"

// third party
#ifndef __APPLE__
#include "omp.h"
#endif

// functions
mjoResult iLQG(void *cbData, trajectory *policy, trajectory **candidates,
               mjOptOpt *options, mjtNum *scratch, int scratchSz);

mjoResult iLQGsolve(void *cbData, trajectory *policy, trajectory **candidates,
                    mjOptOpt *options, trajectory *Tscratch);

int back_pass(trajectory *T, int regType, mjtNum *scratch, int szScratch);

int riccati(int n, int m, mjtNum mu, int regType, const mjtNum *Wx,
            const mjtNum *Wxx, const mjtNum *yx, const mjtNum *yu, const mjtNum *cx,
            const mjtNum *cu, const mjtNum *cxx, const mjtNum *cxu,
            const mjtNum *cuu, const mjtNum *lower, const mjtNum *upper,
            const mjtNum *next_l, mjtNum *Vx, mjtNum *Vxx, mjtNum *du, mjtNum *L,
            mjtNum *dV, mjtNum *Qx, mjtNum *Qu, mjtNum *Qxx, mjtNum *Qxu, mjtNum *Quu,
            mjtNum *scratch);

void makeRollouts(trajectory *policy, trajectory **candidates, mjOptOpt *option,
                  mjtNum t0, const mjtNum *x0);

void prepareCandidates(trajectory *policy, trajectory **candidates,
                       mjOptOpt *option, mjtNum t0, const mjtNum *x0);

int multiRollout(void *cbData, const trajectory *policy,
                 trajectory **candidates);

int trajDerivatives(void *cbData, trajectory *T);

void set_dt(trajectory *T, const mjOptOpt optopt, mjtNum dt);


#endif  // _TT_ILQG_H_
