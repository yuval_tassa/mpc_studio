#ifndef _TT_BOXQP_H_
#define _TT_BOXQP_H_

#include "util.h"

#include "math.h"

int boxQP(int* dim, mjtNum* x, mjtNum** _R, mjtNum** _index,  // outputs
          mjtNum* scratch, const int szScratch,             // scratch space
          const mjtNum* H, const mjtNum* g,                  // quadratic program
          const mjtNum* lower, const mjtNum* upper,          // bounds
          const mjtNum* x0, const mjtNum* options);          // optional inputs

#endif  // _TT_BOXQP_H_
