//#include "mj_matlab.h"
#include "mpc_studio.h"
// #include "features.h"
// #include <math.h>
// #include "cost.h"
// //#include "eengine_util.h"
// #include "util.h"
// //#include "direct.h"
// #include "mujoco_findiff.h"

#include <memory>
// #include "mujoco_solver.h"
// #include "mujoco.h"
//-----------------------------------------------------------------------------------------

#define TM_START
#define TM_RESTART
#define TM_END(i)
#define TM_START1
#define TM_END1(i)

// trajectory-based rollout
mjtNum rollout(const mjModel *m, const mjResidual *residual, mjData *d,
              trajectory *T, const trajectory *policy) {
  int N = T->N,      // number of timesteps
      nx = T->nx,    // dimension of state
      nu = T->nu,    // dimension of control
      nc = T->nc,    // number of cost terms
      nr = T->nr,    // dimension of residual
      ndx = T->ndx,  // dimension of state derivative
      nq = m->nq, nv = m->nv, na = m->na;
  const mjCost *cost = 0;
  mjtNum inf = -mju_log(0.0);

  // Allocate to the heap to avoid creating huge stack frames
  std::unique_ptr<mjDiffSizes> _diffSizes(new mjDiffSizes);
  mjDiffSizes &s = *_diffSizes;

  // variables
  int i;
  trajElement e, enext;

  // init timer
  TM_START

  // clear timers
  mju_zero(d->timer_duration, mjNTIMER);

  // clear flc_f (in case of warmstart)
  mju_zero(d->efc_force, m->njmax);

  if (residual) {
    s = residual->diffSizes;
    cost = residual->cost + residual->active_cost;
  }

  // check policy, get lag
  if (policy) {
    if (nx != policy->nx || nu != policy->nu || nc != policy->nc ||
        nr != policy->nr || ndx != policy->ndx)
      mju_error("trajectory / policy mismatch");
    T->trace.policy_lag = T->e[0].t[0] - policy->e[0].t[0];
  }

  // initialize cost
  T->trace.cost = 0;

  // copy initial state and time into d
  mju_copy(d->qpos, T->e[0].x, nq);
  mju_copy(d->qvel, T->e[0].x + nq, nv);
  mju_copy(d->act, T->e[0].x + nq + nv, na);
  d->time = T->e[0].t[0];

  // === main loop
  for (i = 0; i < N; i++) {
    e = T->e[i];

    // set dt
    // d.timestep	= e.dt[0]; // for when Emo implements it

    // step1
    mj_step1(m, d);

    // get control or disable it
    if (i != N - 1) {
      // add control from policy
      add_control_new(m, d, &s, &e, policy, i, T->feedback, T->time_shift);

      // apply control
      mju_copy(d->ctrl, e.u, nu);
    } else
      mju_zero(d->ctrl, nu);

    // step2a
    TM_RESTART
    if (i != N - 1) mj_fwdActuation(m, d);
    mj_step2a(m, d);
    TM_END(mjTIMER_STEP)

    // get features
    if (residual) {
      // populate with measurements
      mju_copy(d->userdata, e.user, mjMIN(m->nuserdata, T->nuser));
      get_residual(e.r, e.f, m, residual, d, -1);
    }

    if (i != N - 1) {
      // step2b
      mj_step2b(m, d);

      enext = T->e[i + 1];
      // copy state and time
      mju_copy(enext.x, d->qpos, nq);
      mju_copy(enext.x + nq, d->qvel, nv);
      mju_copy(enext.x + nq + nv, d->act, na);
      enext.t[0] = d->time;

      // divergence check:
      if (tt_isBadVec(enext.x, nx)) {
        if (e.c) e.c[0] = inf;
        T->trace.cost = inf;
        return inf;
      }
    }
  }

  // get cost
  T->trace.cost = 0;
  if (cost)
    for (i = 0; i < N; i++)  // Yuval: #pragma omp? probably not
      T->trace.cost += cost_step_derivs(T->e + i, cost, T->nx, 0, T->nu,
                                 i + 1 == N ? 1 : 0, T->e[0].t[0], 0, 0);

  // add timing, convert to milliseconds
  mju_scl(T->trace.timingCB[0], d->timer_duration, 1e-3,
         mjMIN(mjNTIMER, N_CB_TIMER));

  return T->trace.cost;
}

void add_control(const mjModel *m, mjData *d, mjDiffSizes *s, mjtNum *u,
                 const trajectory *policy, mjtNum t, const mjtNum *x, int align,
                 const mjtNum *modifiers, mjtNum *xd) {
  // constants
  int nx, nu, ndx, N;

  // variables
  int i;
  mjtNum feedback = 1, time_shift = 0;
  trajElement e;

  // mj_stack variables
  mjtNum *eu, *eK, *ex, *dx, *utemp;

  mjMARKSTACK
      // quick return
      if (!policy) return;

  if (modifiers) {
    feedback = modifiers[0];
    time_shift = modifiers[1];
  }

  nx = policy->nx, nu = policy->nu, ndx = policy->ndx, N = policy->N;

  eu = mj_stackAlloc(d, nu);
  eK = mj_stackAlloc(d, nu * ndx);
  ex = mj_stackAlloc(d, nx);
  dx = mj_stackAlloc(d, s->ndw);
  utemp = mj_stackAlloc(d, ndx);
  mju_zero(eu, nu);
  mju_zero(dx, s->ndw);

  // initialize e
  for (i = 0; i < N_TRJ; i++) *(&e.t + i) = 0;
  e.u = eu;
  if (feedback) {
    mju_zero(ex, nx);
    mju_zero(eK, nu * ndx);
    e.x = ex;
    e.K = eK;
  }

  interpTrajectory(&e, policy, t - time_shift, align, 0,
                   1);  // try extrapolate = 1 ?

  // add open loop components
  mju_addTo(u, eu, nu);

  // add feedback
  if (feedback) {
    if (xd) mju_copy(xd, ex, nx);

    // dx = x - ex
    mjd_diff(m, s, d, dx, x, ex, 0);

    // utemp = eK * dx
    mju_mulMatVec(utemp, eK, dx, nu, ndx);

    // add feedback
    mju_addToScl(u, utemp, feedback, nu);
  }

#ifdef ttVERBOSE
  if (tt_isBadVec(utemp, nu)) mju_warning("(add_control) delta u is bad");
#endif

  // clamp to limits
  if (policy->lower[0] < policy->upper[0])
    for (i = 0; i < nu; i++)
      u[i] = mju_min(mju_max(u[i], policy->lower[i]), policy->upper[i]);

  mjFREESTACK
}

// yuval: still needs work: get diff function handle, stop using data stack
void add_control_new(const mjModel *m, mjData *d, mjDiffSizes *s,
                     trajElement *e, const trajectory *policy, int index,
                     mjtNum feedback, mjtNum time_shift) {
  // constants
  int nx, nu, ndx, N;
  int extrapolate = 0, ignoreLast = 1;

  // variables
  int i, aligned;
  mjtNum t;
  trajElement e_tmp;

  // mj_stack variables
  mjtNum *eu, *eK, *ex, *dx, *utemp;

  mjMARKSTACK
      // quick return
      if (!policy) return;

  // get relevant sizes
  nx = policy->nx, nu = policy->nu, ndx = policy->ndx, N = policy->N;

  eu = mj_stackAlloc(d, nu);
  eK = mj_stackAlloc(d, nu * ndx);
  ex = mj_stackAlloc(d, nx);
  dx = mj_stackAlloc(d, s->ndw);
  utemp = mj_stackAlloc(d, ndx);
  mju_zero(eu, nu);
  mju_zero(dx, s->ndw);

  // initialize e_tmp in the stack
  for (i = 0; i < N_TRJ; i++) *(&e_tmp.t + i) = 0;
  e_tmp.u = eu;
  if (feedback) {
    mju_zero(ex, nx);
    mju_zero(eK, nu * ndx);
    e_tmp.xk = ex;
    e_tmp.K = eK;
  }

  // do we have precise alignment?
  aligned = (index > -1 && e->t[0] == policy->e[index].t[0]);

  // interpolate policy(t) into e_tmp
  t = e->t[0] - time_shift;
  interpTrajectory(&e_tmp, policy, t, aligned ? index : -1, extrapolate,
                   ignoreLast);  // Yuval: try extrapolate = 1 ?

  // add open loop components
  mju_addTo(e->u, e_tmp.u, nu);

  // add feedback
  if (feedback) {
    // dx = x - xk
    mjd_diff(m, s, d, dx, e->x, e_tmp.xk, 0);

    // utemp = K * dx
    mju_mulMatVec(utemp, eK, dx, nu, ndx);

    // add feedback
    mju_addToScl(e->u, utemp, feedback, nu);

    // save policy

    if (e->K) mju_copy(e->K, e_tmp.K, nu * ndx);
    if (e->xk) mju_copy(e->xk, e_tmp.xk, nx);
  }

#ifdef ttVERBOSE
  if (tt_isBadVec(utemp, nu)) mju_warning("(add_control) delta u is bad");
#endif

  // clamp to limits
  if (policy->lower[0] < policy->upper[0])
    for (i = 0; i < nu; i++)
      e->u[i] = mju_min(mju_max(e->u[i], policy->lower[i]), policy->upper[i]);

  mjFREESTACK
}

int interp_state(const mjModel *m, mjData *d, mjtNum *x,
                 const trajectory *policy, mjtNum t) {
  // constants
  int nx, N;

  // variables
  int i, w;
  mjtNum t_i, weight;
  trajElement *e_left, *e_right;

  // quick return
  if (!policy) {
    return 0;
  }

  nx = policy->nx, N = policy->N;
  mju_zero(x, nx);

  // clamp t_i to time limits
  t_i = mju_min(mju_max(t, policy->e[0].t[0]), policy->e[N - 2].t[0]);

  for (i = 0; i < N - 1; i++) {
    if (policy->e[i].t[0] <= t_i && t_i <= policy->e[i + 1].t[0])
      break;  // t_i is in the segment [i, i+1]
  }
  w = i;

  // left and right segment trajElement's
  e_left = &(policy->e[i]);
  e_right = &(policy->e[i + 1]);

  // weight of e_right (in the real set [0,1])
  if (e_right->t[0] != e_left->t[0])
    weight = (t_i - e_left->t[0]) / (e_right->t[0] - e_left->t[0]);
  else
    weight = 0;

  if (weight) {
    mju_addToScl(x, e_right->x, weight, nx);
  }
  if (1 - weight) {
    mju_addToScl(x, e_left->x, 1 - weight, nx);
  }
  return w;
}

int interp_cost(const mjModel *m, mjData *d, mjtNum *c, const trajectory *policy,
                mjtNum t) {
  // constants
  int nc, N;

  // variables
  int i, w;
  mjtNum t_i, weight;
  trajElement *e_left, *e_right;

  // quick return
  if (!policy) {
    return 0;
  }

  nc = policy->nc, N = policy->N;
  mju_zero(c, nc);

  // clamp t_i to time limits
  t_i = mju_min(mju_max(t, policy->e[0].t[0]), policy->e[N - 2].t[0]);

  for (i = 0; i < N - 1; i++) {
    if (policy->e[i].t[0] <= t_i && t_i <= policy->e[i + 1].t[0])
      break;  // t_i is in the segment [i, i+1]
  }
  w = i;

  // left and right segment trajElement's
  e_left = &(policy->e[i]);
  e_right = &(policy->e[i + 1]);

  // weight of e_right (in the real set [0,1])
  if (e_right->t[0] != e_left->t[0])
    weight = (t_i - e_left->t[0]) / (e_right->t[0] - e_left->t[0]);
  else
    weight = 0;

  if (weight) {
    mju_addToScl(c, e_right->c, weight, nc);
  }
  if (1 - weight) {
    mju_addToScl(c, e_left->c, 1 - weight, nc);
  }
  return w;
}
