
#pragma once

#include "mujoco.h"

#include "trajectory.h"
#include "cost.h"
#include "features.h"

#define N_ROLL_MODS \
  2  // number of rollout modification types 0:feedback scaling; 1:time-shifting

mjtNum rollout(const mjModel* m, const mjResidual* residual, mjData* d,
              trajectory* candidate, const trajectory* policy);

// void add_control(const mjModel *m, mjData* d, mjDiffSizes *s, mjtNum* u, const
// trajectory* policy, mjtNum t, const mjtNum *x, int align, const mjtNum*
// modifiers, mjtNum *xd);
void add_control_new(const mjModel* m, mjData* d, mjDiffSizes* s,
                     trajElement* e, const trajectory* policy, int index,
                     mjtNum feedback, mjtNum time_shift);
int interp_state(const mjModel* m, mjData* d, mjtNum* x,
                 const trajectory* policy, mjtNum t);
int interp_cost(const mjModel* m, mjData* d, mjtNum* c, const trajectory* policy,
                mjtNum t);
