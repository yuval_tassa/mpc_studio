#ifndef _TT_MUJOCO_CALLBACKS_H_
#define _TT_MUJOCO_CALLBACKS_H_

#include "mujoco.h"

#include "features.h"
#include "util.h"

#include "mujoco_rollout.h"
#include "mujoco_findiff.h"


typedef struct _mjoData {
  mjModel* m;
  mjResidual* r;
  mjData** D;
  mjtNum* prior;
} mjoData;

#endif  // _TT_MUJOCO_CALLBACKS_H_
