#include "mpc_studio.h"

// Expose as C API
#ifdef __cplusplus
extern "C" {
#endif

// Custom user features
extern void user_features(const mjModel* model, mjData* data);

#ifdef __cplusplus
}
#endif


// === feature machinery ===
mjtNum get_datum(const mjModel* model, const mjData* data, mjDatum datum,
                const mjtNum* com, const mjtNum* comvel, const mjtNum* momentum,
                const mjtNum* sqrtMinv, mjtNum* f, int* f_index,
                const mjtNum* prow) {
  mjtNum res[6];
  switch (datum.field) {
    case mjFIELD_CONST:
      return 1;
    case mjFIELD_DT:
      return model->opt.timestep;
    case mjFIELD_TIME:
      return data->time;
    case mjFIELD_COM:
      return data->subtree_com[datum.entry];
    case mjFIELD_ENERGY:
      return data->energy[datum.entry];
    case mjFIELD_XVEL:
      mj_objectVelocity(model, data, mjOBJ_BODY, datum.item, res, 0);
      return res[datum.entry];
    case mjFIELD_GEOM_XVEL:
      mj_objectVelocity(model, data, mjOBJ_GEOM, datum.item, res, 0);
      return res[datum.entry];
    case mjFIELD_SITE_XVEL:
      mj_objectVelocity(model, data, mjOBJ_SITE, datum.item, res, 0);
      return res[datum.entry];
    case mjFIELD_XVEL_LOCAL:
      mj_objectVelocity(model, data, mjOBJ_BODY, datum.item, res, 1);
      return res[datum.entry];
    case mjFIELD_GEOM_XVEL_LOCAL:
      mj_objectVelocity(model, data, mjOBJ_GEOM, datum.item, res, 1);
      return res[datum.entry];
    case mjFIELD_SITE_XVEL_LOCAL:
      mj_objectVelocity(model, data, mjOBJ_SITE, datum.item, res, 1);
      return res[datum.entry];
    case mjFIELD_XACC:
      mj_objectAcceleration(model, data, mjOBJ_BODY, datum.item, res, 0);
      return res[datum.entry];
    case mjFIELD_GEOM_XACC:
      mj_objectAcceleration(model, data, mjOBJ_GEOM, datum.item, res, 0);
      return res[datum.entry];
    case mjFIELD_SITE_XACC:
      mj_objectAcceleration(model, data, mjOBJ_SITE, datum.item, res, 0);
      return res[datum.entry];
    case mjFIELD_XACC_LOCAL:
      mj_objectAcceleration(model, data, mjOBJ_BODY, datum.item, res, 1);
      return res[datum.entry];
    case mjFIELD_GEOM_XACC_LOCAL:
      mj_objectAcceleration(model, data, mjOBJ_GEOM, datum.item, res, 1);
      return res[datum.entry];
    case mjFIELD_SITE_XACC_LOCAL:
      mj_objectAcceleration(model, data, mjOBJ_SITE, datum.item, res, 1);
      return res[datum.entry];
    case mjFIELD_SUBCOM:
      return com[datum.buffer_adr];
    case mjFIELD_SUBCOMVEL:
      return comvel[datum.buffer_adr];
    case mjFIELD_MOMENTUM:
      return momentum[datum.buffer_adr];
    case mjFIELD_SQRTMINV:
      return sqrtMinv[datum.buffer_adr];
    case mjFIELD_FILE:
      return prow[datum.item + datum.entry];
    case mjFIELD_FEATURE:
      return f[f_index[datum.item] + datum.entry];
    default:
      return ((mjtNum*)data->buffer)[datum.buffer_adr];
  }
}

extern mjtFeatureOperation operatorList[NUM_OPERATORS];

void evaluate_features(const mjModel* m, mjData* d, const mjFeature* features,
                       int features_size, const mjDatum* datums, int f_capacity,
                       mjtNum* f, int* f_inx, const mjtNum* com,
                       const mjtNum* comvel, const mjtNum* momentum,
                       const mjtNum* sqrtMinv, const mjtNum* prow) {
  int i, j, n_output;
  mjtNum* pbuf;
  const mjtNum *p1, *p2;
  mjFeature feat;
  mjDatum datum;
  f_inx[0] = 0;
  // create feature vector
  for (i = 0; i < features_size; i++) {
    pbuf = f + f_inx[i];
    feat = features[i];
    if (feat.fun == mj_OP_GET_DATUM) {
      for (j = 0; j < feat.datums_size; j++) {
        datum = datums[feat.datums_adr + j];
        pbuf[j] = (get_datum(m, d, datum, com, comvel, momentum, sqrtMinv, f,
                             f_inx, prow) -
                   datum.ref) *
                  datum.coef;
      }
      n_output = feat.datums_size;
    } else {
      if (feat.in1_adr >= i ||
          feat.in2_adr >=
              i)  // if this happens, there is a bug in the interpreter
        mju_error("evaluation error - feature representation is inconsistent\n");

      p1 = f + f_inx[feat.in1_adr];

      p2 = f + f_inx[feat.in2_adr];

      n_output = mjf_operator(pbuf, p1, feat.in1_size, p2, feat.in2_size,
                              (mjtOperatorType)feat.fun, feat.params);
    }

    f_inx[i + 1] = f_inx[i] + n_output;
#ifdef ttVERBOSE
    if (f_inx[i + 1] >= f_capacity)  // if this happens, there is a bug in the
                                     // feature length computation
      mju_error(
          "feature evaluation error - insufficient space was pre-allocated\n");
#endif
    for (j = 0; j < n_output; j++) pbuf[j] = (pbuf[j] - feat.ref) * feat.coef;

#ifdef ttVERBOSE
    if (tt_isBadVec(pbuf, n_output))
      mju_warning_i("(get_features) feature %d is bad\n", i);
#endif
  }
}

void copy_residuals(mjtNum* r, const mjCost* cost, const mjtNum* f,
                    const int* f_index) {
  int i, nr;
  int residual_inx;
  int feature_inx;
  mjtNum* pbuf = r;  // pointer to residual vector to be filled

  // copy relevant parts into residual vector
  for (i = 0; i < cost->nterms; i++) {
    nr = cost->term[i].nr;
    residual_inx = cost->term[i].f_inx;
    if (residual_inx == -1) continue;
    feature_inx = f_index[residual_inx];

    mju_copy(pbuf, f + feature_inx, nr);
    pbuf += nr;
  }
}

void copy_auxf(mjtNum* auxf, const mjCost* cost, const mjtNum* f,
               const int* f_index) {
  int i, nr;
  int f_inx;
  int feature_inx;
  mjtNum* pbuf = auxf;  // pointer to aux features vector to be filled

  if (cost->plot_f_inx > -1) {
    feature_inx = f_index[cost->plot_f_inx];
    mju_copy(pbuf, f + feature_inx, cost->plot_nf);
    pbuf += cost->plot_nf;
  }

  for (i = 0; i < cost->ntransitions; i++) {
    nr = cost->transition[i].nr;
    f_inx = cost->transition[i].f_inx;
    if (f_inx == -1) continue;
    feature_inx = f_index[f_inx];
    mju_copy(pbuf, f + feature_inx, nr);
    pbuf += nr;
  }
  for (i = 0; i < cost->nalters; i++) {
    nr = cost->alter[i].nr;
    f_inx = cost->alter[i].f_inx;
    if (f_inx == -1) continue;
    feature_inx = f_index[f_inx];
    mju_copy(pbuf, f + feature_inx, nr);
    pbuf += nr;
  }

  if (cost->dataTransition.f_inx > -1) {
    nr = cost->dataTransition.nr;
    f_inx = cost->dataTransition.f_inx;
    feature_inx = f_index[f_inx];
    mju_copy(pbuf, f + feature_inx, nr);
    pbuf += nr;
  }
}


void copy_observations_from_id(mjtNum* observations, const mjResidual* residual,
                       const mjtNum* f, const int* f_index, int id) {
  int feature_inx;
  int flen;
  mjtNum* pbuf = observations;  // pointer to aux features vector to be filled

  feature_inx = f_index[residual->observationIndices[id]];
  flen = residual->observationLengths[id];
  mju_copy(pbuf, f + feature_inx, flen);
}

void get_residual(mjtNum* output, mjtNum* observations, const mjModel* m,
                  const mjResidual* residual, mjData* d, int id) {
  mjMARKSTACK
  const mjCost* cost;
  mjtNum *com = 0, *comvel = 0, *momentum = 0, *sqrtMinv = 0;
  mjtNum* f = 0;
  int* f_index = 0;
  if (!residual) return;

  cost = residual->cost + residual->active_cost;

  f = mj_stackAlloc(d, residual->nf * 2);
  f_index =
      (int*)mj_stackAlloc(d, 1 + (int)ceil((double)residual->num_features *
                                           sizeof(int) / sizeof(mjtNum)));

  if (residual->extra_flags & extftSUBTREE) {
    // make extra computations, buffers
    com = mj_stackAlloc(d, m->nbody * 3);
    comvel = mj_stackAlloc(d, m->nbody * 3);
    momentum = mj_stackAlloc(d, m->nbody * 6);
    mj_subtree(m, d, com, comvel, momentum);
  }

  if (residual->extra_flags & extftRNEPOST) mj_rnePostConstraint(m, d);

  if (residual->extra_flags & extftSQRTMINV) {
    sqrtMinv = mj_stackAlloc(d, m->nv * m->nv);
    tt_eye(sqrtMinv, m->nv);
    mj_solveM(m, d, sqrtMinv, sqrtMinv, m->nv);
  }

  // Custom Features
  //user_features(m, d);

  evaluate_features(m, d, residual->features, residual->num_features,
                    residual->datums, residual->nf * 2, f, f_index, com, comvel,
                    momentum, sqrtMinv,
                    residual->pfiledata + residual->currowstart);
  if (output) 
    copy_residuals(output, cost, f, f_index);
  if (observations && (id >=0))
      copy_observations_from_id(observations, residual, f, f_index, id);
  mjFREESTACK
}


//void get_residual_from_id(mjtNum* output, mjtNum* observations, const mjModel* m,
//                          const mjResidual* residual, mjData* d, int id) {
//  mjMARKSTACK
//  const mjCost* cost;
//  mjtNum *com = 0, *comvel = 0, *momentum = 0, *sqrtMinv = 0;
//  mjtNum* f = 0;
//  int* f_index = 0;
//  if (!residual) return;
//
//  cost = residual->cost + residual->active_cost;
//
//  f = mj_stackAlloc(d, residual->nf * 2);
//  f_index =
//      (int*)mj_stackAlloc(d, 1 + (int)ceil((double)residual->num_features *
//                                           sizeof(int) / sizeof(mjtNum)));
//
//  if (residual->extra_flags & extftSUBTREE) {
//    // make extra computations, buffers
//    com = mj_stackAlloc(d, m->nbody * 3);
//    comvel = mj_stackAlloc(d, m->nbody * 3);
//    momentum = mj_stackAlloc(d, m->nbody * 6);
//    mj_subtree(m, d, com, comvel, momentum);
//  }
//
//  if (residual->extra_flags & extftRNEPOST) mj_rnePost(m, d);
//
//  if (residual->extra_flags & extftSQRTMINV) {
//    sqrtMinv = mj_stackAlloc(d, m->nv * m->nv);
//    mju_eye(sqrtMinv, m->nv);
//    mj_backsubM2(m, d, sqrtMinv, sqrtMinv, m->nv);
//  }
//
//  user_features(m, d);
//
//  evaluate_features(m, d, residual->features, residual->num_features,
//                    residual->datums, residual->nf * 2, f, f_index, com, comvel,
//                    momentum, sqrtMinv,
//                    residual->pfiledata + residual->currowstart);
//  if (output) {
//    copy_residuals(output, cost, f, f_index);
//  }
//  if (observations) {  // TODO(marris) Add id
//    copy_observations(observations, residual, f, f_index);
//  }
//  mjFREESTACK
//
//}


// === operators ===

// 0 means "any", -1 means "not applicable"
// {name, nparam, n1, n2, associative, fun}
mjtFeatureOperation operatorList[NUM_OPERATORS] = {
    {"cat", 0, 0, 0, 1, mj_OP_CAT},
    {"sum", 0, 0, 0, 1, mj_OP_SUM},
    {"max", 1, 0, 0, 1, mj_OP_MAX},
    {"loss", 0, 0, 0, 0, mj_OP_LOSS},
    {"norm", 0, 0, 0, 0, mj_OP_NORM},
    {"add", 0, 0, 0, 1, mj_OP_ADD},
    {"prod", 0, 0, 0, 1, mj_OP_PROD},
    {"dot", 0, 0, 0, 0, mj_OP_DOT},
    {"normalize", 0, 0, 0, 0, mj_OP_NORMALIZE},
    {"cross", 0, 3, 3, 0, mj_OP_CROSS},
    {"sincos", 0, 0, 0, 0, mj_OP_SINCOS},
    {"segproj", 1, 6, 3, 0, mj_OP_SEGPROJ},
    {"quatdiff", 0, 4, 4, 0, mj_OP_QUATDIFF},
    {"mulmatvec", 0, 0, 0, 0, mj_OP_MULMATVEC},
    {"mulmattvec", 0, 0, 0, 0, mj_OP_MULMATTVEC}};

int mjf_operator(mjtNum* f, const mjtNum* x, int nx, const mjtNum* y, int ny,
                 mjtOperatorType type, const mjtNum* params) {
  int i, n = mjMAX(nx, ny);
  mjtNum _f = 0;
  mjtNum axis[3], center[3], vec[3], negquat[4], difquat[4], posquat[4], t = 0,
                                                                        length;
  mjtNormType normType;
  f[0] = 0;

#ifdef ttVERBOSE
  if (tt_isBadVec(x, nx)) mju_warning("(mjf_operator) first input is bad\n");
  if (tt_isBadVec(y, ny)) mju_warning("(mjf_operator) second input is bad\n");
#endif

  switch (type) {
    case mj_OP_CAT:  // f = [x; y]
      for (i = 0; i < nx; i++) f[i] = x[i];
      for (i = 0; i < ny; i++) f[nx + i] = y[i];
      return nx + ny;

    case mj_OP_SUM:  // f = sum([x; y])
      for (i = 0; i < nx; i++) f[0] += x[i];

      for (i = 0; i < ny; i++) f[0] += y[i];
      return 1;

    case mj_OP_MAX:  // f = max(x,y)
      if (nx != ny && nx != 1 && ny != 1)
        mju_error("mjf_operator MAX: dimension error!");
      else
        for (i = 0; i < n; i++)
          f[i] = tt_softMax(x[nx == 1 ? 0 : i], y[ny == 1 ? 0 : i], params[0]);
      return n;

    case mj_OP_LOSS:  // f = loss([x; y])
      normType = (mjtNormType)(int)params[0];
      for (i = 0; i < nx; i++)
        f[i] = mj_norm(0, 0, x + i, 1, params + 1, normType);

      for (i = 0; i < ny; i++)
        f[i + nx] = mj_norm(0, 0, y + i, 1, params + 1, normType);
      return nx + ny;

    case mj_OP_NORM:  // f = norm([x; y])
      normType = (mjtNormType)(int)params[0];
      if (funList[type].isloss)
        for (i = 0; i < nx; i++)
          _f += mj_norm(0, 0, x + i, 1, params + 1, normType);
      else {
        mju_copy(f, x, nx);
        mju_copy(f + nx, y, ny);
        _f = mj_norm(0, 0, f, nx + ny, params + 1, normType);
      }
      f[0] = _f;
      return 1;

    case mj_OP_ADD:  // f = x + y
      if (nx != ny && nx != 1 && ny != 1)
        mju_error("mjf_operator ADD: dimension error!");
      else
        for (i = 0; i < n; i++) f[i] = x[nx == 1 ? 0 : i] + y[ny == 1 ? 0 : i];
      return n;

    case mj_OP_PROD:  // f = x .* y
      if (nx != ny && nx != 1 && ny != 1)
        mju_error("mjf_operator PROD: dimension error!");
      else
        for (i = 0; i < n; i++) f[i] = x[nx == 1 ? 0 : i] * y[ny == 1 ? 0 : i];
      return n;

    case mj_OP_DOT:  // f = x' * y
      if (nx != ny && nx != 1 && ny != 1)
        mju_error("mjf_operator DOT: dimension error!");
      else
        for (i = 0; i < mjMAX(nx, ny); i++)
          f[0] += x[nx == 1 ? 0 : i] * y[ny == 1 ? 0 : i];
      return 1;

    case mj_OP_NORMALIZE:  // f = x/norm(x)   or   y.*x/norm(x)
      for (i = 0; i < nx; i++) t += x[i] * x[i];
      t = mju_sqrt(t) + mjMINVAL;
      if (ny == nx)
        for (i = 0; i < nx; i++) f[i] = y[i] * x[i] / t;
      else
        for (i = 0; i < nx; i++) f[i] = x[i] / t;
      return nx;

    case mj_OP_CROSS:  // f = cross(x,y)
      if (nx != 3 && ny != 3) mju_error("mjf_operator CROSS: dimension error!");
      mju_cross(f, x, y);
      return 3;

    case mj_OP_SINCOS:  // f = [sin(x), cos(x)]
      for (i = 0; i < nx; i++) {
        f[2 * i] = mju_sin(x[i]);
        f[2 * i + 1] = mju_cos(x[i]);
      }
      return 2 * nx;

    case mj_OP_MULMATVEC:  // f = mat(x) * y
      if (nx != ny * ny) mju_error("mjf_operator MULMATVEC: dimension error!");
      mju_mulMatVec(f, x, y, ny, ny);
      return ny;

    case mj_OP_MULMATTVEC:  // f = mat(x)' * y
      if (nx != ny * ny) mju_error("mjf_operator MULMATTVEC: dimension error!");
      mju_mulMatTVec(f, x, y, ny, ny);
      return ny;

    case mj_OP_SEGPROJ:  // f = project y(0:2) onto the segment [x(0:2) x(3:5)]
      mju_sub3(axis, x, x + 3);
      length = mju_normalize3(axis) / 2 + params[0];
      mju_add3(center, x, x + 3);
      mju_scl3(center, center, 0.5);
      mju_sub3(vec, y, center);
      // project onto axis
      t = mju_dot3(vec, axis);
      // clamp
      t = mju_max(-length, mju_min(length, t));
      mju_scl3(vec, axis, t);
      mju_add3(f, vec, center);
      return 3;

    case mj_OP_QUATDIFF:  // f = neg(y)*x
      mju_negQuat(negquat, y);
      mju_normalize(negquat, 4);
      mju_copy(posquat, x, 4);
      mju_normalize(posquat, 4);
      mju_mulQuat(difquat, negquat, posquat);
      mju_quat2Vel(f, difquat, 1);
      return 3;

    default:
      mju_error("mjf_operator: unknown operator type");
      return 0;
  }
}

// === i/o functionallity ===

// id used to indentify binary mjModel file/buffer: ID + sizeof(mjtNum)/4
static const int ID = 54321;

// write to memory buffer
static void bufwrite(const void* src, int num, int szbuf, void* buf,
                     int* ptrbuf) {
  // check pointers
  if (!src || !buf || !ptrbuf) mju_error("NULL pointer passed to bufwrite");

  // check size
  if (*ptrbuf + num > szbuf)
    mju_error("attempting to write outside model buffer");

  // write, advance pointer
  memcpy((char*)buf + *ptrbuf, src, num);
  *ptrbuf += num;
}

// read from memory buffer
static void bufread(void* dest, int num, int szbuf, const void* buf,
                    int* ptrbuf) {
  // check pointers
  if (!dest || !buf || !ptrbuf) mju_error("NULL pointer passed to bufread");

  // check size
  if (*ptrbuf + num > szbuf)
    mju_error("attempting to read outside model buffer");

  // read, advance pointer
  memcpy(dest, (char*)buf + *ptrbuf, num);
  *ptrbuf += num;
}

// size of mjResidual+mjtCost
static const int INFOSIZERC = sizeof(mjResidual) + sizeof(mjCost);

// save feature
int mj_saveFC(mjResidual* residual, const char* filename, int szbuf,
              void* buf) {
  int ptrbuf = 0;
  int i;
  FILE* fp = 0;
  int nCosts = residual->ncosts;

  // standard header: id, nCosts, infosize
  int header[3] = {ID + sizeof(mjtNum) / 4, nCosts, INFOSIZERC};

  // open file for writing
  if (szbuf <= 0) {
    fp = fopen(filename, "wb");
    if (!fp) mju_error("could not open file");
  }

  // write standard header, info and buffer (omit pointers)
  if (fp) {
    fwrite(header, 3, sizeof(int), fp);
    fwrite(residual, sizeof(mjResidual), sizeof(char), fp);
    for (i = 0; i < nCosts; i++)
      fwrite(residual->cost + i, sizeof(mjCost), sizeof(char), fp);
  } else {
    bufwrite(header, 3 * sizeof(int), szbuf, buf, &ptrbuf);
    bufwrite(residual, sizeof(mjResidual) * sizeof(char), szbuf, buf, &ptrbuf);
    for (i = 0; i < nCosts; i++)
      bufwrite(residual->cost + i, sizeof(mjCost) * sizeof(char), szbuf, buf,
               &ptrbuf);
  }

  if (fp) fclose(fp);

  return 3 * sizeof(int) + nCosts * sizeof(mjCost) + sizeof(mjResidual);
}

int mj_loadFC(mjResidual* residual, int max_nCosts, const char* filename,
              int szbuf, void* buf) {
  int header[3];
  int nCosts, i;
  int ptrbuf = 0;
  FILE* fp = 0;

  // open file for reading
  if (szbuf <= 0) {
    fp = fopen(filename, "rb");
    if (!fp) mju_error("could not open file");
  }

  // read header
  if (fp)
    fread(header, 3, sizeof(int), fp);
  else
    bufread(header, 3 * sizeof(int), szbuf, buf, &ptrbuf);

  // check header
  if (header[0] != ID + sizeof(mjtNum) / 4 || header[2] != INFOSIZERC) {
    if (header[0] == ID + sizeof(mjtNum) / 4 + 1)
      mju_error("model is DOUBLE percision, executable is SINGLE precision");
    else if (header[0] == ID + sizeof(mjtNum) / 4 - 1)
      mju_error("model is SINGLE percision, executable is DOUBLE precision");
    else
      mju_error("invalid residual/cost");
  }

  nCosts = header[1];
  if (nCosts > max_nCosts)
    mju_error("too many costs received, not enough room made");

  // read structures
  if (fp) {
    fread((void*)residual, 1, sizeof(mjResidual), fp);
    for (i = 0; i < nCosts; i++)
      fread((void*)(residual->cost + i), 1, sizeof(mjCost), fp);
  } else {
    bufread((void*)residual, sizeof(mjResidual), szbuf, buf, &ptrbuf);
    for (i = 0; i < nCosts; i++)
      bufread((void*)(residual->cost + i), sizeof(mjCost), szbuf, buf, &ptrbuf);
  }

  if (fp) fclose(fp);

  return nCosts;
}
