#include "mpc_studio.h"

#include <iostream>
#include <sstream>
#include <fstream>
#include <stdexcept>


static const int nFCF = 41;
static const char* FCF[nFCF][mjXATTRNUM] = {
    {"extensions", "!", "0"},
    {"<"},

    // Features (used for costs and observations)
    {"features", "?", "0"},
    {"<"},
    {"datafile", "?", "1", "filename"},
    {"<"},
    {"item", "*", "3", "name", "columns", "row_offset"},
    {">"},
    {"feature", "*", "1", "name"},
    {"<"},
    {"contact", "*", "9", "field", "geom1", "geom2", "body1", "body2",
     "selector", "entry", "coef", "ref"},
    {"data", "*", "5", "field", "item", "entry", "coef", "ref"},
    {"op", "R", "6", "type", "ref", "coef", "params", "norm", "loss"},
    {"<"},
    {"data", "*", "5", "field", "item", "entry", "coef", "ref"},
    {"contact", "*", "9", "field", "geom1", "geom2", "body1", "body2",
     "selector", "entry", "coef", "ref"},
    {">"},
    {">"},
    {">"},

    // Diffs
    {"diff", "?", "0"},
    {"<"},
    {"ignore_joint", "*", "3", "name", "posvel", "entry"},
    {"ignore_subtree", "*", "1", "name"},
    {"add_tendon", "*", "1", "name"},
    {"remove_constraint", "*", "1", "name"},
    {">"},

    // Costs
    {"costs", "?", "1", "default"},
    {"<"},
    {"cost", "*", "1", "name"},
    {"<"},
    {"term", "*", "6", "name", "feature", "coef", "coef_final", "norm",
     "params"},
    {"transition", "*", "6", "to", "name", "feature", "threshold", "norm",
     "params"},
    {"alter", "*", "4", "name", "field", "item", "feature"},
    {"datatransition", "?", "6", "goto", "name", "feature", "threshold", "norm",
     "params"},
    {">"},
    {">"},

    // Tweaks
    {"tweaks", "?", "0"},
    {"<"},
    {"tweak", "*", "6", "name", "field", "item", "entry", "guitype", "range"},
    {">"},

    {">"}};

//------------------- class mjCError implementation ----------------------------

// constructor
ttError::ttError(const ttBase* obj, const char* msg, const char* str, int pos1,
                 int pos2) {
  char temp[1000];

  // init
  warning = false;
  if (obj || msg)
    sprintf(message, "Error");
  else
    message[0] = 0;

  // construct error message
  if (msg) {
    if (str)
      sprintf(temp, msg, str, pos1, pos2);
    else
      sprintf(temp, msg, pos1, pos2);

    strcat(message, ": ");
    strcat(message, temp);
  }

  // append info from mjCBase object
  if (obj) {
    // with or without xml position
    if (obj->xmlpos[0] >= 0)
      sprintf(temp, "Object name = %s, id = %d, xml_row = %d, xml_col = %d",
              obj->name.c_str(), obj->id, obj->xmlpos[0], obj->xmlpos[1]);
    else
      sprintf(temp, "Object name = %s, id = %d", obj->name.c_str(), obj->id);

    // append to message
    strcat(message, "\n");
    strcat(message, temp);
  }
}


int getNamedNumericFieldIndex(const mjModel* m, const char* field_name) 
{
  int field_index = -1;
  if (m->nnumeric) {
    for (int i = 0; i < m->nnumeric; i++) {
      if (!strcmp(m->names + m->name_numericadr[i], field_name)) {
        field_index = i;
        break;
      }
    }
  }
  return field_index;
}



int getNTimesteps(const mjModel* m) 
{
  const double tol = 1e-11;
  int nTimesteps = 1;

  if (m->nnumeric) {
    // Look for user-defined control timestep.
    int field_index = getNamedNumericFieldIndex(m, "control_timestep");

    if (field_index >= 0) 
    {
      mjtNum control_dt = m->numeric_data[m->numeric_adr[field_index]];
    
      if (control_dt < m->opt.timestep )
        throw std::runtime_error( "Control timestep must not be smaller than integration timestep." );

      if(fabs(remainder(control_dt, m->opt.timestep)) > tol)
        throw std::runtime_error( "Control timestep must be an integer multiple of integration timestep." );

      nTimesteps = (int)(control_dt / m->opt.timestep);
    }
  }

  return nTimesteps;
}


mjCFC* fcParseCompile(const mjModel* m, std::string filename, char* error) {
  mjCFC* fc;
  bool defaultfc = filename.length() == 0;

  // load XML file
  TiXmlDocument doc(filename.c_str());
  if (!doc.LoadFile() && !defaultfc) {
    sprintf(error,
            "XML parse error at line %d, column %d:\n%s\nmodel will be loaded "
            "with an empty cost\n",
            doc.ErrorRow(), doc.ErrorCol(), doc.ErrorDesc());
    defaultfc = true;
  }

  if (defaultfc) {
    printf("no cost file specified, creating an empty one\n");
    fc = new mjCFC;
    fc->m = m;
    mjData* d = mj_makeData(m);
    try {
      fc->compileRC(m, d);
    }
    // catch known errors
    catch (ttError err) {
      strcpy(error, err.message);
      delete fc;
      mj_deleteData(d);
      return 0;
    }
    // hack to prevent crash at traj allocation:
    //fc->residual.cost[0].optopt.N = 2;
    //fc->residual.cost[0].optopt.n_dt_start = 0;
    mj_deleteData(d);
    return fc;
  }

  // get top-level element
  TiXmlElement* root = doc.RootElement();
  if (!root) {
    sprintf(error, "XML root element not found");
    return 0;
  }

  // create model
  fc = new mjCFC;
  fc->m = m;


  // parse with exceptions
  mjXFCF parser;
  try {
    parser.m = m;
    parser.setFC(fc);
    parser.Parse(root);
  }
  // catch known errors
  catch (mjXError err) {
    strcpy(error, err.message);
    delete fc;
    return 0;
  }

  mjData* d = mj_makeData(m);
  try {
    fc->compileRC(m, d);
  }
  // catch known errors
  catch (ttError err) {
    strcpy(error, err.message);
    delete fc;
    mj_deleteData(d);
    return 0;
  }

  mj_deleteData(d);

  // set nTimesteps
  fc->residual.nTimesteps = getNTimesteps(m);

  return fc;
}

mjXFCF::mjXFCF() : schema(FCF, nFCF) {}

void mjXFCF::setFC(mjCFC* _fc) { fc = _fc; }

// main entry point for XML parser
//  mjCModel is allocated here; caller is responsible for deallocation
void mjXFCF::Parse(TiXmlElement* root) {
  // check schema
  if (!schema.GetError().empty())
    throw mjXError(0, "XML Schema Construction Error: %s\n",
                   schema.GetError().c_str());

  // validate
  TiXmlElement* bad = 0;
  if ((bad = schema.Check(root)))
    throw mjXError(bad, "Schema violation: %s\n", schema.GetError().c_str());

  TiXmlElement* section;
  // if( (section = FindSubElem(root, "diff")) )
  //    Diff(section);

  if ((section = FindSubElem(root, "features"))) {
    Features(section);
  }

  if ((section = FindSubElem(root, "costs"))) {
    Cost(section);
  }

  if ((section = FindSubElem(root, "tweaks"))) {
    Tweaks(section);
  }
}

void mjXFCF::Tweaks(TiXmlElement* section) {
  std::string text, type;
  TiXmlElement* tweak;
  double temp[2];
  int itmp;
  // iterate over child elements
  tweak = section->FirstChildElement();
  while (tweak) {
    mjCTweak* ptw = new mjCTweak(m, fc);
    fc->tweaks.push_back(ptw);
    ReadAttrTxt(tweak, "field", text, true);
    ptw->fieldname = text;

    ReadAttrTxt(tweak, "item", text, true);
    ptw->itemname = text;

    ReadAttrTxt(tweak, "entry", text, true);
    ptw->entryname = text;

    text.clear();
    ReadAttrTxt(tweak, "name", text);
    if (text.length())
      ptw->name = text;
    else
      ptw->name = ptw->itemname + ":" + ptw->entryname;

    if (MapValue(section, "guitype", &itmp, gui_map, 1))
      ptw->slider = (itmp == 1);
    else
      ptw->slider = true;

    ReadAttr(tweak, "range", 2, temp, text, true);
    ptw->minval = (float)temp[0];
    ptw->maxval = (float)temp[1];

    tweak = tweak->NextSiblingElement();
  }
}

// ==============  XML PARSER for new features =======================
void mjXFCF::parse_children(std::string funname,
                            std::vector<TiXmlElement*> children,
                            mjCOperator* res) {
  std::string type;
  if (children.size() == 1) {
    type = children[0]->Value();
    if (type == "op")
      res->in2 = parse_operator(children[0]);
    else
      res->in2 = parse_datums(children[0]);
  } else {
    mjCOperator* implicit = new mjCOperator(m, fc);
    implicit->funname = funname;
    res->in2 = implicit;

    type = children[0]->Value();
    if (type == "op")
      implicit->in1 = parse_operator(children[0]);
    else
      implicit->in1 = parse_datums(children[0]);

    for (int i = 1; i < children.size(); i++) children[i - 1] = children[i];

    children.pop_back();

    parse_children(funname, children, implicit);
  }
}

mjCOperator* mjXFCF::parse_operator(TiXmlElement* oper) {
  mjCOperator* op = new mjCOperator(m, fc);
  std::string type, text;
  int np;
  float temp[MAX_NORM_PARAM];

  ReadAttrTxt(oper, "type", type);
  if (type.length()) op->funname = type;

  type.clear();
  ReadAttrTxt(oper, "loss", type);
  if (type.length()) {
    if (op->funname.length())
      throw mjXError(oper,
                     "parse_operator: only one operation may be specified, "
                     "either as a ''loss'', a ''norm'', or a generic ''type''");
    op->funname = type;
    op->lossNorm = mjNL_LOSS;
  }

  type.clear();
  ReadAttrTxt(oper, "norm", type);
  if (type.length()) {
    if (op->funname.length())
      throw mjXError(oper,
                     "parse_operator: only one operation may be specified, "
                     "either as a ''loss'', a ''norm'', or a generic ''type''");
    op->funname = type;
    op->lossNorm = mjNL_NORM;
  }

  if (!op->funname.length())
    throw mjXError(oper,
                   "parse_operator: an operator must specify either a type, a "
                   "loss or a norm");

  np = ReadAttr(oper, "params", MAX_NORM_PARAM, temp, text, false, false);
  for (int i = 0; i < np; i++) op->params.push_back(temp[i]);
  ReadAttr(oper, "ref", 1, &op->ref, text, false, true);
  ReadAttr(oper, "coef", 1, &op->coef, text, false, true);

  TiXmlElement* child;
  std::vector<TiXmlElement*> children;
  child = oper->FirstChildElement();
  if (!child) throw mjXError(child, "an operator must have at least one child");
  // plug in the first child to the first place
  type = child->Value();
  if (type == "op")
    op->in1 = parse_operator(child);
  else
    op->in1 = parse_datums(child);

  child = child->NextSiblingElement();
  while (child) {
    children.push_back(child);
    child = child->NextSiblingElement();
  }

  if (children.size())  // TODO: error if fun is non-associative and more than
                        // one child remains
    parse_children(op->funname, children, op);

  return op;
}

#define MAX_DATUMS 100
mjCFeatureComponent* mjXFCF::parse_datums(TiXmlElement* datums) {
  std::string name;
  int np;
  float temp[MAX_DATUMS];
  std::string type = datums->Value();
  if (type == "data") {
    mjCDatums* res = new mjCDatums(m, fc);
    ReadAttrTxt(datums, "field", name, true);
    res->fieldname = name;
    name.clear();
    ReadAttrTxt(datums, "item", name);
    res->itemnames = name;
    name.clear();
    ReadAttrTxt(datums, "entry", name);
    res->entrynames = name;
    name.clear();
    np = ReadAttr(datums, "coef", MAX_DATUMS, temp, name, false, false);
    for (int i = 0; i < np; i++) res->coefs.push_back(temp[i]);
    np = ReadAttr(datums, "ref", MAX_DATUMS, temp, name, false, false);
    for (int i = 0; i < np; i++) res->refs.push_back(temp[i]);
    return res;
  } else  // fake else to get rid of warning
  {
    mjCDatums* res = new mjCDatums(m, fc);
    return res;
  }

  /*
  else
  {
      mjCContact* res = new mjCContact(model);

      ReadAttrTxt(datums, "field", name, true);
      res->fieldname=name;
      name.clear();
      name.clear();
      ReadAttrTxt(datums, "selector", name);
      res->selectorname=name;
      name.clear();
      ReadAttrTxt(datums, "entry", name);
      res->entrynames=name;
      name.clear();
      np=ReadAttr(datums, "coef", MAX_DATUMS, temp, name,false, false);
      for( int i=0;i<np;i++ )
          res->coefs.push_back(temp[i]);
      np=ReadAttr(datums, "ref", MAX_DATUMS, temp, name,false, false);
      for( int i=0;i<np;i++ )
          res->refs.push_back(temp[i]);

      ReadAttrTxt(datums, "geom1", name);
      if( name.size() )
      {
          res->geom1name=name;
          // test for body1, throw error if found
          name.clear();
          ReadAttrTxt(datums, "body1", name);
          if( name.size() )
              throw mjXError(datums,"both body1 and geom1?!");
      }
      else
      {
          ReadAttrTxt(datums, "body1", name);
          if( !name.size() )
              throw mjXError(datums,"either body1 or geom1!");
          else
              res->body1name=name;
      }

      name.clear();
      ReadAttrTxt(datums, "geom2", name);
      if( name.size() )
      {
          res->geom2name=name;
          // test for body1, throw error if found
          name.clear();
          ReadAttrTxt(datums, "body2", name);
          if( name.size() )
              throw mjXError(datums,"both body2 and geom2?!");
      }
      else
      {
          ReadAttrTxt(datums, "body2", name);
          if( name.size() )
              res->body2name=name;
      }

      return res;

  }
  */
}

void mjXFCF::Datafile(TiXmlElement* section) {
  // parse name, filename

  // register columns with the model
}

void mjXFCF::Features(TiXmlElement* section) {
  std::string text, type;
  TiXmlElement *content, *feature;
  mjCFeature* pfeat;

  fc->rowlength = 0;
  // iterate over child elements
  feature = section->FirstChildElement();
  while (feature) {
    text = feature->Value();
    // read name
    ReadAttrTxt(feature, "name", text, true);
    pfeat = fc->AddFeature(text);
    content = feature->FirstChildElement();
    if (content->NextSiblingElement()) {
      // a feature with multiple children - implicit concatenation!
      mjCOperator* res = new mjCOperator(m, fc);
      pfeat->component = res;
      res->funname = "cat";

      type = content->Value();
      if (type == "data")
        res->in1 = parse_datums(content);
      else
        res->in1 = parse_operator(content);

      TiXmlElement* child;
      std::vector<TiXmlElement*> children;
      child = content->NextSiblingElement();
      while (child) {
        children.push_back(child);
        child = child->NextSiblingElement();
      }

      parse_children("cat", children, res);
    } else {
      type = content->Value();
      if (type == "data")
        pfeat->component = parse_datums(content);
      else
        pfeat->component = parse_operator(content);
    }
    feature = feature->NextSiblingElement();
  }
}

// engine section parser
void mjXFCF::OptOpt(TiXmlElement* section, mjOptOpt* popt) {
  std::string text;
  int n;
  float temp[3];

  // read options
  if (ReadAttr(section, "logmurange", 2, temp, text, false, true)) {
    popt->muMin = powf(10, temp[0]);
    popt->muMax = powf(10, temp[1]);
  }
  if (ReadAttr(section, "alpharange", 2, temp, text, false, true)) {
    popt->alphaBad = temp[0];
    popt->alphaGood = temp[1];
  }
  if (ReadAttr(section, "zrange", 2, temp, text, false, true)) {
    popt->zBad = temp[0];
    popt->zGood = temp[1];
  }
  if (ReadAttr(section, "vardt", 3, temp, text, false, true)) {
    popt->dt_start = temp[0];
    popt->n_dt_start = (int)temp[1];
    popt->n_dt_interp = (int)temp[2];
  }
  ReadAttr(section, "mufactor", 1, &popt->muFactor, text);

  ReadAttrInt(section, "horizon", &popt->N);
  ReadAttrInt(section, "nthreads", &popt->nThreads);
  ReadAttrInt(section, "logeps", &popt->log2eps);
  ReadAttrInt(section, "maxbpass", &popt->maxBpass);
  ReadAttrInt(section, "nrollout", &popt->nRollout);
  ReadAttrInt(section, "itercloud", &popt->iter_flc_f);
  if (MapValue(section, "xreg", &n, bool_map, 2)) popt->regType = (n == 1);
  if (MapValue(section, "extrapcontact", &n, bool_map, 2))
    popt->extrap_lc_dist = (n == 1);
  if (MapValue(section, "exactact", &n, bool_map, 2)) popt->act_jac = (n == 1);
  if (MapValue(section, "central", &n, bool_map, 2)) popt->central = (n == 1);

  // if( popt->maxiter<1 )
  //	throw mjXError(section, "maxiter must be greater than 0");

  // LM-specific:
  ReadAttrInt(section, "maxiter", &popt->maxIter);
  ReadAttr(section, "muinit", 1, &popt->muInit, text);
  ReadAttr(section, "tolx", 1, &popt->tolX, text);
  ReadAttr(section, "tolg", 1, &popt->tolG, text);
  ReadAttr(section, "tolfun", 1, &popt->tolFun, text);
  if (MapValue(section, "sparseHess", &n, bool_map, 2))
    popt->sparseHess = (n == 1);
}


//------------------------- COST / FEATURES --------------

// cost section parser
void mjXFCF::Cost(TiXmlElement* section) {
  std::string text, type, name, cost_default, cost_name;
  TiXmlElement *elem, *costelem;
  double params[MAX_NORM_PARAM];
  int np, i;
  mjCCost_term* pterm;
  mjCCost* pcost;
  mjCTransition* pTR;
  mjCAlter* pAL;

  mjOption mpc_opt;
  // mj_defaultOption(&mpc_opt);
  mpc_opt = m->opt;

// Loop through costs' child elements
  costelem = section->FirstChildElement();
  while (costelem) {
      // Only pay attention to cost child elements
      name = costelem->Value();
      if (name == "cost") {
        ReadAttrTxt(costelem, "name", text, true);
        fc->costNames.push_back(text);

        pcost = fc->AddCost(text);

        text.clear();
        //ReadAttrTxt(costelem, "observation", text, false);
        //if (text.length()) pcost->plot_feature_name = text;


        // iterate over individual cost terms
        elem = costelem->FirstChildElement();
        while (elem) {
          // name
          name = elem->Value();

          if (name == "datatransition") {
            // transition:
            pcost->datafiletransition = new mjCTransition(m, fc);
            mjCTransition* pTR = pcost->datafiletransition;
            ReadAttr(elem, "threshold", 1, &pTR->threshold, text, true);

            // inheritance would solve this code copy
            ReadAttrTxt(elem, "feature", text, true);
            pTR->feature_name = text;
            ReadAttrTxt(elem, "name", text);  // might or might not change "text"
            pTR->name = text;
            if (pTR->feature_name == "timeout") {
              // flipping the sign!
              pTR->threshold *= -1;
            } else {
              ReadAttrTxt(elem, "norm", text, true);
              pTR->normname = text;

              double params[MAX_NORM_PARAM];
              int np = ReadAttr(elem, "params", MAX_NORM_PARAM, params, text,
                                false, false);
              for (int i = 0; i < np; i++) pTR->params.push_back(params[i]);
            }

          } else if (name == "transition") {
            pTR = new mjCTransition(m, fc);
            pcost->transitions.push_back(pTR);
            ReadAttrTxt(elem, "to", text, true);
            pTR->to_name = text;
            ReadAttr(elem, "threshold", 1, &pTR->threshold, text, true);

            // inheritance would solve this code copy
            ReadAttrTxt(elem, "feature", text, true);
            pTR->feature_name = text;
            ReadAttrTxt(elem, "name", text);  // might or might not change "text"
            pTR->name = text;
            if (pTR->feature_name == "timeout") {
              // flipping the sign!
              pTR->threshold *= -1;
            } else {
              ReadAttrTxt(elem, "norm", text, true);
              pTR->normname = text;

              np = ReadAttr(elem, "params", MAX_NORM_PARAM, params, text, false,
                            false);
              for (i = 0; i < np; i++) pTR->params.push_back(params[i]);
            }

          } else if (name == "term") {
            pterm = new mjCCost_term(m, fc);
            pcost->terms.push_back(pterm);

            ReadAttr(elem, "coef", 1, pterm->coef, text);
            ReadAttr(elem, "coef_final", 1, pterm->coef + 1, text);

            // inheritance would solve this code copy
            ReadAttrTxt(elem, "feature", text, true);
            pterm->featurename = text;
            ReadAttrTxt(elem, "name", text);  // might or might not change "text"
            pterm->name = text;

            ReadAttrTxt(elem, "norm", text, true);
            pterm->normname = text;

            np = ReadAttr(elem, "params", MAX_NORM_PARAM, params, text, false,
                          false);
            for (i = 0; i < np; i++) pterm->params.push_back(params[i]);

          } else if (name == "alter") {
            pAL = new mjCAlter(m);
            pcost->alters.push_back(pAL);

            ReadAttrTxt(elem, "field", name, true);
            pAL->fieldname = name;
            name.clear();
            ReadAttrTxt(elem, "item", name);
            pAL->itemname = name;
            name.clear();
            // inheritance would solve this code copy
            ReadAttrTxt(elem, "feature", text, true);
            pAL->feature_name = text;
            ReadAttrTxt(elem, "name", text);  // might or might not change "text"
            pAL->name = text;
          }

          // advance to next term
          elem = elem->NextSiblingElement();
        }
      }
    // advance to next element
    costelem = costelem->NextSiblingElement();
  } // close loop over costs
}
/*
// diff section parser
void mjXFCF::Diff(TiXmlElement* section)
{
    std::string text, type;
    TiXmlElement *elem;
    mjCIgnore *pig;

    // iterate over child elements
    elem = section->FirstChildElement();
    while( elem )
    {
        type = elem->Value();
        pig = model->AddIgnore();
        if( type=="ignore_joint" )
        {
            pig->type=0;
            ReadAttrTxt(elem, "name", pig->name, true);
            pig->posvel=0;
            ReadAttrTxt(elem, "posvel", text, false);
            if( text.length()>0 )
                if( text=="qpos" )
                    pig->posvel=1;
                else if( text=="qvel" )
                    pig->posvel=2;
                else
                    throw mjXError(elem, "the only supported values for posvel
are qpos and qvel, got %s", text.c_str(), 0);

            ReadAttrTxt(elem, "entry", pig->entryname, false);
        }
        else if( type=="ignore_subtree" )
        {
            pig->type=1;
            ReadAttrTxt(elem, "name", pig->name, true);
        }
        else if( type=="add_tendon" )
        {
            pig->type=2;
            ReadAttrTxt(elem, "name", pig->name, true);
        }
        else if( type=="remove_constraint" )
        {
        }
        // advance to next element
        elem = elem->NextSiblingElement();
    }

}
*/
