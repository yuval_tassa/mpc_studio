#ifndef _TT_XML_H_
#define _TT_XML_H_

#include "xml_base.h"
#include "user_cost.h"
#include "tinystr.h"
#include "tinyxml.h"

// GUI type
static const mjMap gui_map[2] = {{"textbox", 0}, {"slider", 1}};

// bool type
const mjMap bool_map[2] = {{"false", 0}, {"true", 1}};

class mjXFCF : public mjXBase {
  friend class mjCTweak;

 public:
  mjXFCF();  // constructor
  void setFC(mjCFC* _fc);
  void Parse(TiXmlElement* root);  // parse XML document

 protected:
  mjCFC* fc;

 private:
  void Features(TiXmlElement* section);  // features section
  void Datafile(TiXmlElement* section);  // external section (inside features)
  void Observation(TiXmlElement* section);  // cost section
  void Cost(TiXmlElement* section);         // cost section
  void OptOpt(TiXmlElement* section, mjOptOpt* popt);
  void Tweaks(TiXmlElement* section);

  mjCFeatureComponent* parse_datums(TiXmlElement*);
  mjCOperator* parse_operator(TiXmlElement*);
  void parse_children(std::string funname, std::vector<TiXmlElement*> children,
                      mjCOperator* res);

  mjXSchema schema;  // schema used for validation
};

mjCFC* fcParseCompile(const mjModel* m, std::string filename, char* error);

#endif  // _TT_XML_H_
